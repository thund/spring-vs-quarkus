package de.tobi6112.mm.inventoryservice.domain.entities.artist;

import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "artist_credit")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class ArtistCredit {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "artist_count")
    private Short artistCount;

    @Column(name = "ref_count")
    private Integer refCount;

    @Column(name = "created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "edits_pending")
    private Integer editsPending;

    @OneToMany
    @JoinColumns({
        @JoinColumn(name = "artist_credit", referencedColumnName = "id")
    })
    private Set<ArtistCreditName> artistCreditNames;

}
