package de.tobi6112.mm.inventoryservice.domain.dto.artist;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.Artist;
import de.tobi6112.mm.inventoryservice.util.DateUtil;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Value;

@Value
public class ArtistDto {
  UUID gid;
  String name;
  String startDate;
  String endDate;
  String type;
  String area;
  String gender;
  String comment;
  Boolean ended;
  Set<ArtistAliasDto> aliases;

  public static ArtistDto fromArtist(Artist artist) {
    var gid = artist.getGid();
    var name = artist.getName();
    var startDate = DateUtil.createDateStringFromFragments(artist.getBeginDateYear(),
        artist.getBeginDateMonth(), artist.getBeginDateDay());
    var endDate = DateUtil.createDateStringFromFragments(artist.getEndDateYear(),
        artist.getEndDateMonth(), artist.getEndDateDay());
    var type = Optional.ofNullable(artist.getType()).map(t -> t.getName()).orElse(null);
    var area = Optional.ofNullable(artist.getArea()).map(a -> a.getName()).orElse(null);
    var gender = Optional.ofNullable(artist.getGender()).map(g -> g.getName()).orElse(null);
    var comment = artist.getComment();
    var ended = artist.getEnded();
    var aliases = Optional.ofNullable(artist.getAliases()).map(a -> a.stream().map(alias -> ArtistAliasDto.fromArtistAlias(alias)).collect(
        Collectors.toSet())).orElse(Collections.emptySet());
    return new ArtistDto(gid, name, startDate, endDate, type, area, gender, comment, ended, aliases);
  }
}
