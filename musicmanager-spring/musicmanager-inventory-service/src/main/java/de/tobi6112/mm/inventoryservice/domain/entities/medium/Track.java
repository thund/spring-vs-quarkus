package de.tobi6112.mm.inventoryservice.domain.entities.medium;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.ArtistCredit;
import de.tobi6112.mm.inventoryservice.domain.entities.recording.Recording;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "track")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class Track {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "gid", unique = true)
    private UUID gid;

    @JoinColumn(name = "recording", referencedColumnName = "id")
    @ManyToOne
    private Recording recording;

    @JoinColumn(name = "medium", referencedColumnName = "id")
    @ManyToOne
    private Medium medium;

    @Column(name = "position")
    private Integer position;

    @Column(name = "number")
    private String number;

    @Column(name = "name")
    private String name;

    @JoinColumn(name = "artist_credit", referencedColumnName = "id")
    @ManyToOne
    private ArtistCredit artistCredit;

    @Column(name = "length")
    private Integer length;

    @Column(name = "edits_pending")
    private Integer editsPending;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @Column(name = "is_data_track")
    private Boolean isDataTrack;

}
