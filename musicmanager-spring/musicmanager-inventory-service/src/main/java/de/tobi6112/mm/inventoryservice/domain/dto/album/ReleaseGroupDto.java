package de.tobi6112.mm.inventoryservice.domain.dto.album;

import de.tobi6112.mm.inventoryservice.domain.entities.release.ReleaseGroup;
import java.util.Optional;
import lombok.Value;

@Value
public class ReleaseGroupDto {
  String name;
  String type;

  public static ReleaseGroupDto fromReleaseGroup(ReleaseGroup releaseGroup) {
    var name = releaseGroup.getName();
    var type = Optional.ofNullable(releaseGroup.getType()).map(t -> t.getName()).orElse(null);
    return new ReleaseGroupDto(name, type);
  }
}
