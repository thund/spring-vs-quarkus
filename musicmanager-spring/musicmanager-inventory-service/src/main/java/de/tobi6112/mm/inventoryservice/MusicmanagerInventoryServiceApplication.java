package de.tobi6112.mm.inventoryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicmanagerInventoryServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(MusicmanagerInventoryServiceApplication.class, args);
  }

}
