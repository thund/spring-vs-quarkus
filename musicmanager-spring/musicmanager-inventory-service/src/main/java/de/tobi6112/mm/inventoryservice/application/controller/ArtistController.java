package de.tobi6112.mm.inventoryservice.application.controller;

import de.tobi6112.mm.inventoryservice.application.service.ArtistService;
import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("artists")
public class ArtistController {

  private final ArtistService artistService;

  @Autowired
  public ArtistController(ArtistService artistService) {
    this.artistService = artistService;
  }

  @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<ArtistDto> getArtistById(@PathVariable UUID id) {
    return this.artistService.findByGid(id)
        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Artist with id '" + id +"' not found")));
  }

  @GetMapping(value = "{id}/albums", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<AlbumDto>> getAlbumsOfArtistById(@PathVariable UUID id) {
    return artistService.findAllReleasesByArtistGid(id)
        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Artist with id '" + id +"' not found")));
  }

  @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<ArtistDto>> findByName(@RequestParam(name = "q") String query,
      @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestParam(name = "limit", required = false, defaultValue = "25") Integer limit) {
    return artistService.findAllByName(query, page, limit);
  }
}
