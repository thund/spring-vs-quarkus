package de.tobi6112.mm.inventoryservice.domain.entities.artist;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "artist_alias")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class ArtistAlias {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "artist")
    private Integer artist;

    @Column(name = "name")
    private String name;

    @Column(name = "locale")
    private String locale;

    @Column(name = "edits_pending")
    private Integer editsPending;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @JoinColumn(name = "type", referencedColumnName = "id")
    @ManyToOne
    private ArtistAliasType type;

    @Column(name = "sort_name")
    private String sortName;

    @Column(name = "begin_date_year")
    private Short beginDateYear;

    @Column(name = "begin_date_month")
    private Short beginDateMonth;

    @Column(name = "begin_date_day")
    private Short beginDateDay;

    @Column(name = "end_date_year")
    private Short endDateYear;

    @Column(name = "end_date_month")
    private Short endDateMonth;

    @Column(name = "end_date_day")
    private Short endDateDay;

    @Column(name = "primary_for_locale")
    private Boolean primaryForLocale;

    @Column(name = "ended")
    private Boolean ended;

}
