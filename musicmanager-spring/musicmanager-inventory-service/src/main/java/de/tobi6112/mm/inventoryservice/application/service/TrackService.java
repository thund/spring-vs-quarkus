package de.tobi6112.mm.inventoryservice.application.service;

import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import de.tobi6112.mm.inventoryservice.domain.repositories.RecordingRepository;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
public class TrackService {

  private final RecordingRepository recordingRepository;

  @Autowired
  public TrackService(
      RecordingRepository recordingRepository) {
    this.recordingRepository = recordingRepository;
  }

  public Mono<TrackDto> getTrackByGid(UUID gid) {
    return Mono.fromCallable(() -> recordingRepository.findByGid(gid))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .map(TrackDto::fromRecording);
  }

  public Mono<List<ArtistDto>> getArtistsOfTrackByGid(UUID gid) {
    return Mono.fromCallable(() -> recordingRepository.findAllArtistsByGid(gid))
        .subscribeOn(Schedulers.boundedElastic())
        .map(artists -> artists.stream().map(ArtistDto::fromArtist).collect(Collectors.toList()));
  }

  public Mono<Boolean> existsTrackByGid(UUID gid) {
    return Mono.fromCallable(() -> recordingRepository.existsByGid(gid));
  }

  public Mono<List<TrackDto>> findAllByName(String name, Integer page, Integer limit) {
    return Mono.fromCallable(() -> recordingRepository.findAllByNameContainingIgnoreCase(name,
        PageRequest.of(page, limit)))
        .subscribeOn(Schedulers.boundedElastic())
        .map(tracks -> tracks.stream().map(TrackDto::fromRecording).collect(Collectors.toList()));
  }

  public Mono<Boolean> existsTrackByGid(List<UUID> gid) {
    return Mono.fromCallable(() -> recordingRepository.countAllByGidIn(gid) == gid.size());
  }
}
