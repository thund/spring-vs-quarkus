package de.tobi6112.mm.inventoryservice.domain.entities.release;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.ArtistCredit;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "release")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class Release {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "gid", unique = true)
    private UUID gid;

    @Column(name = "name")
    private String name;

    @JoinColumn(name = "artist_credit", referencedColumnName = "id")
    @ManyToOne
    private ArtistCredit artistCredit;

    @JoinColumn(name = "release_group", referencedColumnName = "id")
    @ManyToOne
    private ReleaseGroup releaseGroup;

    @JoinColumn(name = "status", referencedColumnName = "id")
    @ManyToOne
    private ReleaseStatus status;

    @JoinColumn(name = "packaging", referencedColumnName = "id")
    @ManyToOne
    private ReleasePackaging packaging;

    @JoinColumn(name = "language", referencedColumnName = "id")
    @ManyToOne
    private Language language;

    @JoinColumn(name = "script", referencedColumnName = "id")
    @ManyToOne
    private Script script;

    @Column(name = "barcode")
    private String barcode;

    @Column(name = "comment")
    private String comment;

    @Column(name = "edits_pending")
    private Integer editsPending;

    @Column(name = "quality")
    private Short quality;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

}
