package de.tobi6112.mm.inventoryservice.domain.entities.artist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "artist_credit_name")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class ArtistCreditName implements Serializable {
    @EmbeddedId
    private ArtistCreditNameId id;
    /*@Id
    @Column(name = "artist_credit")
    private Integer artistCreditId;*/

    /*@ManyToOne
    @JoinColumn(name = "artist_credit", referencedColumnName = "id")
    private ArtistCredit artistCredit;*/

    /*@Id
    @Column(name = "position")
    private Short position;*/

    @JoinColumn(name = "artist", referencedColumnName = "id")
    @ManyToOne
    private Artist artist;

    @Column(name = "name")
    private String name;

    @Column(name = "join_phrase")
    private String joinPhrase;

}
