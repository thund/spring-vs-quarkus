package de.tobi6112.mm.inventoryservice.domain.dto.search;

import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import java.util.Set;
import lombok.Value;

@Value
public class SearchResultDto {
  Set<AlbumDto> albums;
  Set<TrackDto> tracks;
  Set<ArtistDto> artists;
}
