package de.tobi6112.mm.inventoryservice.domain.repositories;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.Artist;
import de.tobi6112.mm.inventoryservice.domain.entities.recording.Recording;
import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordingRepository extends JpaRepository<Recording, Integer> {
  Optional<Recording> findByGid(UUID gid);

  boolean existsByGid(UUID gid);

  int countAllByGidIn(List<UUID> gid);

  @Query("SELECT a FROM Recording rec join rec.artistCredit ac join ac.artistCreditNames acn join acn.artist a where rec.gid = ?1")
  List<Artist> findAllArtistsByGid(UUID gid);

  List<Recording> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
}
