package de.tobi6112.mm.inventoryservice.domain.repositories;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.Artist;
import de.tobi6112.mm.inventoryservice.domain.entities.recording.Recording;
import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReleaseRepository extends JpaRepository<Release, Integer> {
  Optional<Release> findByGid(UUID gid);

  @Query("SELECT a FROM Release r join r.artistCredit ac join ac.artistCreditNames acn join acn.artist a where r.gid = ?1")
  List<Artist> findAllArtistsOfRelease(UUID id);

  @Query("SELECT rec FROM Track t join t.recording rec join t.medium m join m.release r where r.gid = ?1")
  List<Recording> findAllRecordingsOfRelease(UUID id);

  List<Release> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
}
