package de.tobi6112.mm.inventoryservice.domain.repositories;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.Artist;
import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Integer> {
  Optional<Artist> findByGid(UUID gid);

  @Query("SELECT r FROM Release r join r.releaseGroup rg join rg.artistCredit ac join ac.artistCreditNames acn join acn.artist a where a.gid = ?1")
  List<Release> findAllReleasesByArtistGid(UUID id);

  List<Artist> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
}
