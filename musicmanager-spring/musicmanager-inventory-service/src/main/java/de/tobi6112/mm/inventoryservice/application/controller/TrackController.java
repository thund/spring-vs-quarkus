package de.tobi6112.mm.inventoryservice.application.controller;

import de.tobi6112.mm.inventoryservice.application.service.TrackService;
import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("tracks")
public class TrackController {
  private final TrackService trackService;

  @Autowired
  public TrackController(
      TrackService trackService) {
    this.trackService = trackService;
  }

  @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<TrackDto> getTrackByGid(@PathVariable UUID id) {
    return trackService.getTrackByGid(id)
        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Track with id '" + id +"' not found")));
  }

  @RequestMapping(method = RequestMethod.HEAD, value = "{id}")
  public Mono<ResponseEntity<Void>> existsById(@PathVariable UUID id) {
    return trackService.existsTrackByGid(id)
        .map(exists -> {
          if(exists) {
            return ResponseEntity.ok().build();
          }
          return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        });
  }

  @GetMapping(value = "{id}/artists", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<ArtistDto>> getArtistsOfTrackByGid(@PathVariable UUID id) {
    return trackService.getArtistsOfTrackByGid(id)
        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Track with id '" + id +"' not found")));
  }

  @RequestMapping(method = RequestMethod.HEAD, value = "checkTracks")
  public Mono<ResponseEntity<Void>> existsById(@RequestParam(name = "id") List<UUID> id) {
    return trackService.existsTrackByGid(id)
        .map(exists -> exists ? ResponseEntity.ok().build() : ResponseEntity.status(HttpStatus.NOT_FOUND).build());
  }

  @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<TrackDto>> findByName(@RequestParam(name = "q") String query,
      @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestParam(name = "limit", required = false, defaultValue = "25") Integer limit) {
    return trackService.findAllByName(query, page, limit);
  }
}
