package de.tobi6112.mm.inventoryservice.util;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtil {
  public static String createDateStringFromFragments(Short year, Short month, Short day) {
    try {
      if(year != null && month != null && day != null) {
        var date = LocalDate.of(year, month, day);
        var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return formatter.format(date);
      }
      if(year != null && month != null) {
        var date = LocalDate.of(year, month, 1);
        var formatter = DateTimeFormatter.ofPattern("yyyy-MM");
        return formatter.format(date);
      }
      if(year != null) {
        var date = LocalDate.of(year, 1, 1);
        var formatter = DateTimeFormatter.ofPattern("yyyy");
        return formatter.format(date);
      }
    } catch (DateTimeException e) {
      e.printStackTrace();
    }
    return null;
  }
}
