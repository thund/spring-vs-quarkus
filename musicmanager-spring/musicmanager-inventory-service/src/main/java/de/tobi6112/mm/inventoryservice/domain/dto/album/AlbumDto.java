package de.tobi6112.mm.inventoryservice.domain.dto.album;

import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistAliasDto;
import de.tobi6112.mm.inventoryservice.domain.entities.artist.Artist;
import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import de.tobi6112.mm.inventoryservice.util.DateUtil;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Value;

@Value
public class AlbumDto {
  UUID gid;
  String name;
  String status;
  ReleaseGroupDto releaseGroup;
  LanguageDto language;

  public static AlbumDto fromRelease(Release release) {
    var gid = release.getGid();
    var name = release.getName();
    var status = Optional.ofNullable(release.getStatus()).map(s -> s.getName()).orElse(null);
    var releaseGroup = Optional.ofNullable(release.getReleaseGroup()).map(r -> ReleaseGroupDto.fromReleaseGroup(r)).orElse(null);
    var language = Optional.ofNullable(release.getLanguage()).map(l -> LanguageDto.fromLanguage(l)).orElse(null);

    return new AlbumDto(gid, name, status, releaseGroup,language);
  }
}
