package de.tobi6112.mm.inventoryservice.application.service;

import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import de.tobi6112.mm.inventoryservice.domain.repositories.ArtistRepository;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
public class ArtistService {

  private final ArtistRepository artistRepository;

  @Autowired
  public ArtistService(ArtistRepository artistRepository) {
    this.artistRepository = artistRepository;
  }

  public Mono<ArtistDto> findByGid(UUID gid) {
    return Mono.fromCallable(() -> artistRepository.findByGid(gid))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .map(ArtistDto::fromArtist);
  }

  public Mono<List<AlbumDto>> findAllReleasesByArtistGid(UUID gid) {
    return Mono.fromCallable(() -> artistRepository.findAllReleasesByArtistGid(gid))
        .subscribeOn(Schedulers.boundedElastic())
        .map(albums -> albums.stream().map(AlbumDto::fromRelease).collect(Collectors.toList()));
  }

  public Mono<List<ArtistDto>> findAllByName(String name, Integer page, Integer limit) {
    return Mono.fromCallable(() -> artistRepository.findAllByNameContainingIgnoreCase(name,
        PageRequest.of(page, limit)))
        .subscribeOn(Schedulers.boundedElastic())
        .map(artists -> artists.stream().map(ArtistDto::fromArtist).collect(Collectors.toList()));
  }
}
