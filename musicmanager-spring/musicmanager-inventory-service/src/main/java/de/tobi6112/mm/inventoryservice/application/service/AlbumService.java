package de.tobi6112.mm.inventoryservice.application.service;

import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import de.tobi6112.mm.inventoryservice.domain.repositories.ReleaseRepository;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Service
public class AlbumService {

  private final ReleaseRepository releaseRepository;

  @Autowired
  public AlbumService(ReleaseRepository releaseRepository) {
    this.releaseRepository = releaseRepository;
  }

  public Mono<AlbumDto> findAlbumByGid(UUID gid) {
    return Mono.fromCallable(() -> releaseRepository.findByGid(gid))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .map(AlbumDto::fromRelease);
  }

  public Mono<List<ArtistDto>> findArtistsOfAlbumByGid(UUID gid) {
    return Mono.fromCallable(() -> releaseRepository.findAllArtistsOfRelease(gid))
        .subscribeOn(Schedulers.boundedElastic())
        .map(artists -> artists.stream().map(ArtistDto::fromArtist).collect(Collectors.toList()));
  }

  public Mono<List<TrackDto>> findTracksOfAlbumByGid(UUID gid) {
    return Mono.fromCallable(() -> releaseRepository.findAllRecordingsOfRelease(gid))
        .subscribeOn(Schedulers.boundedElastic())
        .map(tracks -> tracks.stream().map(TrackDto::fromRecording).collect(Collectors.toList()));
  }

  public Mono<List<AlbumDto>> findAllByName(String name, Integer page, Integer limit) {
    return Mono.fromCallable(() -> releaseRepository.findAllByNameContainingIgnoreCase(name, PageRequest.of(page, limit)))
        .subscribeOn(Schedulers.boundedElastic())
        .map(albums -> albums.stream().map(AlbumDto::fromRelease).collect(Collectors.toList()));
  }
}
