package de.tobi6112.mm.inventoryservice.domain.entities.release;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "script")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class Script {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "iso_code", unique = true, columnDefinition = "bpchar")
    private String isoCode;

    @Column(name = "iso_number", columnDefinition = "bpchar")
    private String isoNumber;

    @Column(name = "name")
    private String name;

    @Column(name = "frequency")
    private Short frequency;
}
