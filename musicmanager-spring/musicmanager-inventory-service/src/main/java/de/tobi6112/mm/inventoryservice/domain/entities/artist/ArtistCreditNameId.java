package de.tobi6112.mm.inventoryservice.domain.entities.artist;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class ArtistCreditNameId implements Serializable {
  @ManyToOne
  @JoinColumn(name = "artist_credit", referencedColumnName = "id")
  private ArtistCredit artistCredit;

  @Column(name = "position")
  private Short position;

}
