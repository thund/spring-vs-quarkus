package de.tobi6112.mm.inventoryservice.application.controller;

import de.tobi6112.mm.inventoryservice.application.service.AlbumService;
import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("albums")
public class AlbumController {
  private final AlbumService albumService;

  @Autowired
  public AlbumController(
      AlbumService albumService) {
    this.albumService = albumService;
  }

  @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<AlbumDto> getAlbumById(@PathVariable UUID id) {
    return albumService.findAlbumByGid(id)
        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Album with id '" + id +"' not found")));
  }

  @GetMapping(value = "{id}/artists", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<ArtistDto>> getArtistsOfAlbumByGid(@PathVariable UUID id) {
    return albumService.findArtistsOfAlbumByGid(id)
        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Album with id '" + id +"' not found")));
  }

  @GetMapping(value = "{id}/tracks", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<TrackDto>> getTracksOfAlbumByGid(@PathVariable UUID id) {
    return albumService.findTracksOfAlbumByGid(id)
        .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Album with id '" + id +"' not found")));
  }

  @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<AlbumDto>> findByName(@RequestParam(name = "q") String query,
      @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestParam(name = "limit", required = false, defaultValue = "25") Integer limit) {
    return albumService.findAllByName(query, page, limit);
  }
}
