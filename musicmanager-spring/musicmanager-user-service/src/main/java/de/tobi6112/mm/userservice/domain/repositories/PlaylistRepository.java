package de.tobi6112.mm.userservice.domain.repositories;

import de.tobi6112.mm.userservice.domain.entities.Playlist;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaylistRepository extends JpaRepository<Playlist, UUID> {
  List<Playlist> findAllByNameContainingIgnoreCase(String name);
}