package de.tobi6112.mm.userservice.domain.dto;

import de.tobi6112.mm.userservice.domain.entities.Playlist;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Value;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PlaylistDto {
  private UUID id;
  private String name;
  private Boolean publicPlaylist;
  private Boolean collaborative;

  public static PlaylistDto fromPlaylist(Playlist playlist) {
    return new PlaylistDto(playlist.getId(), playlist.getName(), playlist.getPublicPlaylist(), playlist.getCollaborative());
  }
}
