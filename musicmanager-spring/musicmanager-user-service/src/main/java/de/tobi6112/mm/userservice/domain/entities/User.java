package de.tobi6112.mm.userservice.domain.entities;

import de.tobi6112.mm.userservice.domain.dto.UserDto;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "mm_users")
public class User {
  @Id
  @Setter(AccessLevel.NONE)
  private UUID id = UUID.randomUUID();

  @Setter(AccessLevel.NONE)
  @Column(unique = true)
  private String username;

  private String displayName;

  public User(String username, String displayName) {
    this.username = username;
    this.displayName = displayName;
  }

  public static User fromDto(UserDto userDto) {
    return new User(userDto.getUsername(), userDto.getDisplayName());
  }
}
