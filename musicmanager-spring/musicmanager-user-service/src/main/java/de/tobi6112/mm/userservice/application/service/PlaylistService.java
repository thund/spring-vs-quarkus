package de.tobi6112.mm.userservice.application.service;

import de.tobi6112.mm.userservice.application.net.InventoryService;
import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.entities.Playlist;
import de.tobi6112.mm.userservice.domain.entities.PlaylistEntry;
import de.tobi6112.mm.userservice.domain.repositories.PlaylistRepository;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Slf4j
@Service
public class PlaylistService {
  private final PlaylistRepository playlistRepository;
  private final InventoryService inventoryService;

  @Autowired
  public PlaylistService(
      PlaylistRepository playlistRepository, InventoryService inventoryService) {
    this.playlistRepository = playlistRepository;
    this.inventoryService = inventoryService;
  }

  public Mono<PlaylistDto> findPlaylistWithId(UUID id) {
    return Mono.fromCallable(() -> playlistRepository.findById(id))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .switchIfEmpty(
            Mono.error(new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Playlist with id '" + id +"' not found")))
        .map(PlaylistDto::fromPlaylist);
  }

  @Transactional
  public Mono<PlaylistDto> updatePlaylist(UUID id, PlaylistDto playlistDto) {
    return Mono.fromCallable(() -> playlistRepository.findById(id))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .switchIfEmpty(
            Mono.error(new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Playlist with id '" + id +"' not found")))
        .map(playlist -> {
          playlist.setPublicPlaylist(playlistDto.getPublicPlaylist());
          playlist.setCollaborative(playlistDto.getCollaborative());
          playlist.setName(playlistDto.getName());
          return playlistRepository.save(playlist);
        }).map(PlaylistDto::fromPlaylist);
  }

  @Transactional
  public void deletePlaylist(UUID id) {
    playlistRepository.deleteById(id);
  }

  @Transactional
  public Mono<List<PlaylistEntry>> findTracksOfPlaylist(UUID id) {
    return Mono.fromCallable(() -> playlistRepository.findById(id))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .switchIfEmpty(
            Mono.error(new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Playlist with id '" + id +"' not found")))
        .map(playlist -> playlist.getTracks().stream().collect(Collectors.toList()));
  }

  @Transactional
  public Mono<List<PlaylistEntry>> addTracksToPlaylist(UUID id, Collection<PlaylistEntry> tracks) {
    return Mono.fromCallable(() -> playlistRepository.findById(id))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .switchIfEmpty(
            Mono.error(new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Playlist with id '" + id +"' not found")))
        .filterWhen(it -> inventoryService.allTracksExistById(tracks.stream().map(PlaylistEntry::getTrack).collect(
            Collectors.toList())))
        .map(playlist -> {
          tracks.stream().forEach(playlist::addTrack);
          return playlistRepository.save(playlist);
        })
        .map(playlist -> playlist.getTracks().stream().collect(Collectors.toList()));
  }

  @Transactional
  public Mono<Boolean> deleteTracksFromPlaylist(UUID id, Collection<UUID> trackIds) {
    return Mono.fromCallable(() -> playlistRepository.findById(id))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .switchIfEmpty(
        Mono.error(new ResponseStatusException(
            HttpStatus.NOT_FOUND, "Playlist with id '" + id +"' not found")))
        .map(playlist -> {
          trackIds.stream().forEach(playlist::removeTrack);
          return playlistRepository.save(playlist);
        })
        .map(playlist -> true);
  }

  public Mono<List<PlaylistDto>> searchPublicPlaylist(String query) {
    return Mono.fromCallable(() -> playlistRepository.findAllByNameContainingIgnoreCase(query))
        .subscribeOn(Schedulers.boundedElastic())
        .map(playlists -> playlists.stream().map(PlaylistDto::fromPlaylist).collect(Collectors.toList()));
  }
}
