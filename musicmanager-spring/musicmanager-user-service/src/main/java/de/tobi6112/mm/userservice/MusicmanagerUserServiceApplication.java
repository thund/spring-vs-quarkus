package de.tobi6112.mm.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicmanagerUserServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(MusicmanagerUserServiceApplication.class, args);
  }

}
