package de.tobi6112.mm.userservice.application.net;

import java.util.List;
import java.util.UUID;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class InventoryService {
  private final WebClient client = WebClient
      .builder()
      .baseUrl("http://musicmanager-inventory:8080/api/v1")
      .build();

  public Mono<Boolean> trackExistsById(UUID id) {
    return client.method(HttpMethod.HEAD)
        .uri("/tracks/{id}", id)
        .exchangeToMono(res -> {
          if(res.statusCode().equals(HttpStatus.OK)) {
            return Mono.just(true);
          }
          if(res.statusCode().equals(HttpStatus.NOT_FOUND)) {
            return Mono.just(false);
          }

          return Mono.error(new RuntimeException("Unexpected Response"));
        }).onErrorReturn(false);
  }

  public Mono<Boolean> allTracksExistById(List<UUID> id) {
    return client.method(HttpMethod.HEAD)
        .uri(uriBuilder ->
          uriBuilder.path("/tracks/checkTracks")
              .queryParam("id", id)
            .build()
        )
        .exchangeToMono(res -> {
          if(res.statusCode().equals(HttpStatus.OK)) {
            return Mono.just(true);
          }
          if(res.statusCode().equals(HttpStatus.NOT_FOUND)) {
            return Mono.just(false);
          }

          return Mono.error(new RuntimeException("Unexpected Response"));
        }).onErrorReturn(false);
  }
}
