package de.tobi6112.mm.userservice.application.service;

import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.dto.UserDto;
import de.tobi6112.mm.userservice.domain.entities.Playlist;
import de.tobi6112.mm.userservice.domain.entities.User;
import de.tobi6112.mm.userservice.domain.repositories.PlaylistRepository;
import de.tobi6112.mm.userservice.domain.repositories.UserRepository;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
public class UserService {
  private final UserRepository userRepository;
  private final PlaylistRepository playlistRepository;

  @Autowired
  public UserService(UserRepository userRepository, PlaylistRepository playlistRepository) {
    this.userRepository = userRepository;
    this.playlistRepository = playlistRepository;
  }

  public Mono<UserDto> findUserWith(String username) {
    return Mono.fromCallable(() -> userRepository.findByUsername(username))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .switchIfEmpty(Mono.error(new ResponseStatusException(
            HttpStatus.NOT_FOUND, "User with username '" + username +"' not found")))
        .map(UserDto::fromUser);
  }

  public Mono<List<PlaylistDto>> findPlaylistsByUser(String username) {
    return Mono.fromCallable(() -> userRepository.findAllPlaylistsByUser(username))
        .subscribeOn(Schedulers.boundedElastic())
        .switchIfEmpty(Mono.error(new ResponseStatusException(
            HttpStatus.NOT_FOUND, "User with username '" + username +"' not found")))
        .map(playlists -> playlists.stream().map(PlaylistDto::fromPlaylist).collect(Collectors.toList()));
  }

  @Transactional
  public Mono<PlaylistDto> createPlaylist(String username, PlaylistDto playlistDto) {
    return Mono.fromCallable(() -> userRepository.findByUsername(username))
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap(optional -> optional.map(Mono::just).orElseGet(Mono::empty))
        .switchIfEmpty(Mono.error(new ResponseStatusException(
            HttpStatus.NOT_FOUND, "User with username '" + username +"' not found")))
        .map(u -> PlaylistDto.fromPlaylist(playlistRepository.save(Playlist.fromDto(playlistDto, u))));
  }

  @Transactional
  public Mono<UserDto> createNewUser(UserDto userDto) {
    return Mono.fromCallable(() -> userRepository.save(User.fromDto(userDto)))
        .subscribeOn(Schedulers.boundedElastic())
        .map(UserDto::fromUser);
  }
}
