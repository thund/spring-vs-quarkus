package de.tobi6112.mm.userservice.domain.repositories;

import de.tobi6112.mm.userservice.domain.entities.Playlist;
import de.tobi6112.mm.userservice.domain.entities.User;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, UUID> {
  @Query("SELECT p from Playlist p join p.user u where u.username = ?1")
  List<Playlist> findAllPlaylistsByUser(String username);

  Optional<User> findByUsername(String username);
}