package de.tobi6112.mm.userservice.application.controller;

import de.tobi6112.mm.userservice.application.net.InventoryService;
import de.tobi6112.mm.userservice.application.service.PlaylistService;
import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.entities.PlaylistEntry;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("playlists")
public class PlaylistController {
  private final PlaylistService playlistService;

  @Autowired
  public PlaylistController(
      PlaylistService playlistService) {
    this.playlistService = playlistService;
  }

  @GetMapping("{id}")
  public Mono<PlaylistDto> getPlaylistById(@PathVariable UUID id) {
    return playlistService.findPlaylistWithId(id);
  }

  @PutMapping("{id}")
  public Mono<PlaylistDto> editPlaylistById(@PathVariable UUID id, @RequestBody PlaylistDto playlistDto) {
    return playlistService.updatePlaylist(id, playlistDto);
  }

  @DeleteMapping("{id}")
  public Mono<ResponseEntity<Void>> deletePlaylistWithId(@PathVariable UUID id) {
    playlistService.deletePlaylist(id);
    return Mono.just(ResponseEntity.status(HttpStatus.NO_CONTENT).build());
  }

  @GetMapping("{id}/tracks")
  public Mono<List<PlaylistEntry>> getTracksOfPlaylist(@PathVariable UUID id) {
    return playlistService.findTracksOfPlaylist(id);
  }

  @PostMapping("{id}/tracks")
  public Mono<List<PlaylistEntry>> addTracksToPlaylist(@PathVariable UUID id, @RequestBody PlaylistEntry[] tracks) {
    return playlistService.addTracksToPlaylist(id, Arrays.asList(tracks));
  }

  @DeleteMapping("{id}/tracks")
  public Mono<ResponseEntity<Void>> deleteTracksFromPlaylist(@PathVariable UUID id, @RequestParam(name = "id") List<UUID> ids) {
    return playlistService.deleteTracksFromPlaylist(id, ids)
        .map(res -> ResponseEntity.status(HttpStatus.NO_CONTENT).build());
  }

  @GetMapping("search")
  public Mono<List<PlaylistDto>> searchPlaylists(@RequestParam String q) {
    return playlistService.searchPublicPlaylist(q);
  }
}
