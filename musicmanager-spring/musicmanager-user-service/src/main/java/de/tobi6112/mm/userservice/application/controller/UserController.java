package de.tobi6112.mm.userservice.application.controller;

import de.tobi6112.mm.userservice.application.service.UserService;
import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.dto.UserDto;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("users")
public class UserController {
  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public Mono<ResponseEntity<UserDto>> createNewUser(@RequestBody UserDto userDto) {
    return userService.createNewUser(userDto)
        .map(user -> ResponseEntity.status(HttpStatus.CREATED).body(user));
  }

  @GetMapping(value = "{username}", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<UserDto> getUserById(@PathVariable String username) {
    return userService.findUserWith(username);
  }

  @GetMapping(value = "{username}/playlists", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<List<PlaylistDto>> getUserPlaylistsById(@PathVariable String username) {
    return userService.findPlaylistsByUser(username);
  }

  @PostMapping(value = "{username}/playlists", produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<ResponseEntity<PlaylistDto>> postUserPlaylist(@PathVariable String username, @RequestBody PlaylistDto playlist) {
    return userService.createPlaylist(username, playlist).map(it -> ResponseEntity.status(HttpStatus.CREATED).body(it));
  }
}
