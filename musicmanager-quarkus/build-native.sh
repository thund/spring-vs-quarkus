#!/usr/bin/env sh

./mvnw package -Pnative \
  '-Dquarkus.native.container-build=true' \
  '-Dquarkus.container-image.build=true' \
  '-Dquarkus.container-image.name=${quarkus.application.name:unset}-native'