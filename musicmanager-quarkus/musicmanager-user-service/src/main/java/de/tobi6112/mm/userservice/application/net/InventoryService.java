package de.tobi6112.mm.userservice.application.net;

import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/api/v1")
@RegisterRestClient(configKey = "mm-inventory-api")
public interface InventoryService {
  @HEAD
  @Path("/tracks/{id}")
  Uni<Response> checkTrackExistsById(@PathParam("id") UUID id);

  @HEAD
  @Path("/tracks/checkTracks")
  Uni<Response> checkTrackExistsByIdList(@QueryParam("id") List<UUID> id);
}
