package de.tobi6112.mm.userservice.domain.entities;

import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@EqualsAndHashCode
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Playlist {
  @Id
  @Setter(AccessLevel.NONE)
  private UUID id = UUID.randomUUID();

  private String name;
  private Boolean publicPlaylist = false;
  private Boolean collaborative = false;

  @ManyToOne
  private User user;

  // TODO not eager
  @ElementCollection(fetch = FetchType.EAGER)
  @Setter(AccessLevel.NONE)
  @OrderBy("position ASC")
  private Set<PlaylistEntry> tracks = Collections.emptySortedSet();

  public Playlist(String name, Boolean publicPlaylist, Boolean collaborative,
      User user, Set<PlaylistEntry> tracks) {
    this.name = name;
    this.publicPlaylist = publicPlaylist;
    this.collaborative = collaborative;
    this.user = user;
    this.tracks = tracks;
  }

  public static Playlist fromDto(PlaylistDto dto, User user) {
    var collaborative = false;
    var publicPlaylist = false;
    if(dto.getCollaborative() != null) {
      collaborative = dto.getCollaborative();
    }
    if(dto.getPublicPlaylist() != null) {
      publicPlaylist = dto.getPublicPlaylist();
    }
    return new Playlist(dto.getName(), collaborative, publicPlaylist, user, Collections.emptySortedSet());
  }

  public PlaylistEntry addTrack(PlaylistEntry playlistEntry) {
    if(tracks.add(playlistEntry)) {
      return playlistEntry;
    }
    return null;
  }

  public void removeTrack(UUID trackId) {
    tracks.removeIf(entry -> entry.getTrack().equals(trackId));
  }
}
