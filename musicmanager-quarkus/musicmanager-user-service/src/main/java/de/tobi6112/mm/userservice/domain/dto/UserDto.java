package de.tobi6112.mm.userservice.domain.dto;

import de.tobi6112.mm.userservice.domain.entities.User;
import lombok.Value;

@Value
public class UserDto {
  String username;
  String displayName;

  public static UserDto fromUser(User user) {
    return new UserDto(user.getUsername(), user.getDisplayName());
  }
}
