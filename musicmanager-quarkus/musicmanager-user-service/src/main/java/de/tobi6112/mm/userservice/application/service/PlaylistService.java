package de.tobi6112.mm.userservice.application.service;

import de.tobi6112.mm.userservice.application.net.InventoryService;
import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.entities.PlaylistEntry;
import de.tobi6112.mm.userservice.domain.repositories.PlaylistRepository;
import io.smallrye.mutiny.Uni;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

@ApplicationScoped
public class PlaylistService {

  private static final Logger LOG = Logger.getLogger(PlaylistService.class);

  @Inject
  PlaylistRepository playlistRepository;

  @Inject
  @RestClient
  InventoryService inventoryService;

  public Uni<PlaylistDto> findPlaylistWithId(UUID id) {
    return Uni.createFrom().item(() -> playlistRepository.findById(id))
        .map(PlaylistDto::fromPlaylist);
  }

  @Transactional
  public Uni<PlaylistDto> updatePlaylist(UUID id, PlaylistDto playlistDto) {
    return Uni.createFrom().item(() -> playlistRepository.findById(id))
        .map(playlist -> {
          playlist.setPublicPlaylist(playlistDto.getPublicPlaylist());
          playlist.setCollaborative(playlistDto.getCollaborative());
          playlist.setName(playlistDto.getName());
          return playlist;
        })
        .invoke(playlist -> this.playlistRepository.persist(playlist))
        .map(PlaylistDto::fromPlaylist);
  }

  @Transactional
  public Uni<Boolean> deletePlaylist(UUID id) {
    return Uni.createFrom().item(() -> playlistRepository.deleteById(id));
  }

  public Uni<List<PlaylistEntry>> findTracksOfPlaylist(UUID id) {
    return Uni.createFrom().item(() -> playlistRepository.findById(id))
        .onItem().ifNull().failWith(new WebApplicationException("Playlist Not Found", Status.NOT_FOUND))
        .onItem().ifNotNull().transform(playlist -> playlist.getTracks().stream().collect(Collectors.toList()));
  }

  @Transactional
  public Uni<List<PlaylistEntry>> addTracksToPlaylist(UUID id, Collection<PlaylistEntry> tracks) {
    return Uni.createFrom().item(() -> this.playlistRepository.findById(id))
        .onItem().ifNull().failWith(new WebApplicationException("Playlist Not Found", Status.NOT_FOUND))
        .flatMap(playlist1 -> this.inventoryService.checkTrackExistsByIdList(tracks.stream().map(PlaylistEntry::getTrack).collect(Collectors.toList()))
              .invoke(() -> tracks.stream().forEach(playlist1::addTrack))
              .invoke(() -> this.playlistRepository.persist(playlist1))
              .map(it -> playlist1.getTracks().stream().collect(Collectors.toList()))
        );

    /*var validTracks = Multi.createFrom().iterable(tracks)
        .select().when(
            track -> this.inventoryService.checkTrackExistsById(track.getTrack())
                .onItemOrFailure().transform((item, failure) -> failure != null || item != null && item.getStatus() == 200
                )).collect().asList();

    return Uni.combine().all().unis(validTracks, playlist)
        .asTuple()
        .onItem().transform(tuple -> {
          tuple.getItem1().forEach(tuple.getItem2()::addTrack);
          return tuple.getItem2();
        })
        .onItem().invoke(playlist1 -> this.playlistRepository.persist(playlist1))
        .onItem().transform(playlist1 -> playlist1.getTracks().stream().collect(Collectors.toList()));*/
  }

  @Transactional
  public Uni<Boolean> deleteTracksFromPlaylist(UUID id, Collection<UUID> trackIds) {
    return Uni.createFrom().item(() -> this.playlistRepository.findById(id))
        .onItem().ifNull().failWith(new WebApplicationException("Playlist Not Found", Status.NOT_FOUND))
        .onItem().ifNotNull()
        .invoke(playlist -> {
          trackIds.stream().forEach(track -> playlist.removeTrack(track));
          this.playlistRepository.persist(playlist);
        })
        .onItem().ifNotNull().transform(r -> true);
  }

  public Uni<List<PlaylistDto>> searchPublicPlaylist(String query) {
    return Uni.createFrom().item(() -> playlistRepository.findAllByNameContainingIgnoreCaseAndPublicPlaylist(query))
        .onItem().ifNull().failWith(new WebApplicationException("Playlist Not Found", Status.NOT_FOUND))
        .onItem().ifNotNull().transform(playlists -> playlists.stream().map(PlaylistDto::fromPlaylist).collect(
            Collectors.toList()));
  }
}
