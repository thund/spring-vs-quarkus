package de.tobi6112.mm.userservice.application.controller;

import de.tobi6112.mm.userservice.application.service.UserService;
import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.dto.UserDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("users")
public class UserController {
  @Inject
  UserService userService;

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Uni<Response> createNewUser(UserDto userDto) {
    return userService.createNewUser(userDto)
        .map(user -> Response.status(Status.CREATED).entity(user).build());
  }

  @GET
  @Path("{username}")
  @Produces(MediaType.APPLICATION_JSON)
  public Uni<UserDto> getUserById(@PathParam("username") String username) {
    return userService.findUserWith(username);
  }

  @GET
  @Path("{username}/playlists")
  @Produces(MediaType.APPLICATION_JSON)
  public Uni<List<PlaylistDto>> getUserPlaylistsById(@PathParam("username") String username) {
    return userService.findPlaylistsByUser(username);
  }

  @POST
  @Path("{username}/playlists")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Uni<Response> postUserPlaylist(@PathParam("username") String username, PlaylistDto playlist) {
    return userService.createPlaylist(username, playlist).map(playlistDto -> Response.status(Status.CREATED).entity(playlistDto).build());
  }
}
