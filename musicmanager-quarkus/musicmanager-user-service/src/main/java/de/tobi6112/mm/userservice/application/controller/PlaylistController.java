package de.tobi6112.mm.userservice.application.controller;

import de.tobi6112.mm.userservice.application.service.PlaylistService;
import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.entities.PlaylistEntry;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("playlists")
public class PlaylistController {
  @Inject
  PlaylistService playlistService;

  @GET
  @Path("{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Uni<PlaylistDto> getPlaylistById(@PathParam("id") UUID id) {
    return playlistService.findPlaylistWithId(id);
  }

  @PUT
  @Path("{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Uni<PlaylistDto> editPlaylistById(@PathParam("id") UUID id, PlaylistDto playlistDto) {
    return playlistService.updatePlaylist(id, playlistDto);
  }

  @DELETE
  @Path("{id}")
  public Uni<Response> deletePlaylistWithId(@PathParam("id") UUID id) {
    return playlistService.deletePlaylist(id).onItem().transform(res -> {
      if(res) {
        return Response.noContent().build();
      }
      return Response.status(Status.NOT_FOUND).build();
    });
  }

  @GET
  @Path("{id}/tracks")
  @Produces(MediaType.APPLICATION_JSON)
  public Uni<List<PlaylistEntry>> getTracksOfPlaylist(@PathParam("id") UUID id) {
    return playlistService.findTracksOfPlaylist(id);
  }

  @POST
  @Path("{id}/tracks")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Uni<List<PlaylistEntry>> addTracksToPlaylist(@PathParam("id") UUID id, List<PlaylistEntry> tracks) {
    return playlistService.addTracksToPlaylist(id, tracks);
  }

  @DELETE
  @Path("{id}/tracks")
  public Uni<Response> deleteTracksFromPlaylist(@PathParam("id") UUID id, @QueryParam("id") List<UUID> ids) {
    return playlistService.deleteTracksFromPlaylist(id, ids).onItem().transform(res -> {
      if(res) {
        return Response.noContent().build();
      }
      return Response.status(Status.NOT_FOUND).build();
    });
  }

  @GET
  @Path("search")
  @Produces(MediaType.APPLICATION_JSON)
  public Uni<List<PlaylistDto>> searchPlaylists(@QueryParam("q") String q) {
    return playlistService.searchPublicPlaylist(q);
  }
}
