package de.tobi6112.mm.userservice.domain.dto;

import de.tobi6112.mm.userservice.domain.entities.Playlist;
import java.util.UUID;
import lombok.Value;

@Value
public class PlaylistDto {
  UUID id;
  String name;
  Boolean publicPlaylist;
  Boolean collaborative;

  public static PlaylistDto fromPlaylist(Playlist playlist) {
    return new PlaylistDto(playlist.getId(), playlist.getName(), playlist.getPublicPlaylist(), playlist.getCollaborative());
  }
}
