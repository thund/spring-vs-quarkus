package de.tobi6112.mm.userservice.application.service;

import de.tobi6112.mm.userservice.domain.dto.PlaylistDto;
import de.tobi6112.mm.userservice.domain.dto.UserDto;
import de.tobi6112.mm.userservice.domain.entities.Playlist;
import de.tobi6112.mm.userservice.domain.entities.User;
import de.tobi6112.mm.userservice.domain.repositories.PlaylistRepository;
import de.tobi6112.mm.userservice.domain.repositories.UserRepository;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

@ApplicationScoped
public class UserService {
  @Inject
  UserRepository userRepository;

  @Inject
  PlaylistRepository playlistRepository;

  public Uni<UserDto> findUserWith(String username) {
    return Uni.createFrom().item(() -> userRepository.findByUsername(username))
        .onItem().ifNull().failWith(new WebApplicationException("User not found", Status.NOT_FOUND))
        .onItem().ifNotNull().transform(UserDto::fromUser);
  }

  public Uni<List<PlaylistDto>> findPlaylistsByUser(String username) {
    return Uni.createFrom().item(() -> playlistRepository.findAllPlaylistsByUser(username))
        .onItem().ifNull().failWith(new WebApplicationException("User not found", Status.NOT_FOUND))
        .onItem().ifNotNull().transform(playlists -> playlists.stream().map(PlaylistDto::fromPlaylist).collect(Collectors.toList()));
  }

  @Transactional
  public Uni<PlaylistDto> createPlaylist(String username, PlaylistDto playlistDto) {
    return Uni.createFrom().item(() -> userRepository.findByUsername(username))
        .map(user -> Playlist.fromDto(playlistDto, user))
        .invoke(playlist -> playlistRepository.persist(playlist))
        .map(playlist -> PlaylistDto.fromPlaylist(playlist));
  }

  @Transactional
  public Uni<UserDto> createNewUser(UserDto userDto) {
    return Uni.createFrom().item(userDto)
        .map(dto -> User.fromDto(dto))
        .invoke(user -> userRepository.persist(user))
        .map(user -> UserDto.fromUser(user));
  }
}
