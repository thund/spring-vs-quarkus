package de.tobi6112.mm.userservice.domain.repositories;

import de.tobi6112.mm.userservice.domain.entities.Playlist;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PlaylistRepository implements PanacheRepositoryBase<Playlist, UUID> {

  public List<Playlist> findAllPlaylistsByUser(String username) {
    return this.find("select p from Playlist p join p.user u where u.username = ?1", username)
        .list();
  }

  public List<Playlist> findAllByNameContainingIgnoreCaseAndPublicPlaylist(String name) {
    return this.find("select p from Playlist p where lower(p.name) like concat('%', lower(?1), '%')", name)
        .list();
  }
}