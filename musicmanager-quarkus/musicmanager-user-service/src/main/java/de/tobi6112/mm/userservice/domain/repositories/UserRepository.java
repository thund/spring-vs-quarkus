package de.tobi6112.mm.userservice.domain.repositories;

import de.tobi6112.mm.userservice.domain.entities.User;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.smallrye.mutiny.Uni;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepositoryBase<User, UUID> {

  public User findByUsername(String username) {
    return this.find("username", username).firstResult();
  }
}