package de.tobi6112.mm.inventoryservice.application.service;

import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import de.tobi6112.mm.inventoryservice.domain.repositories.ArtistRepository;
import de.tobi6112.mm.inventoryservice.domain.repositories.RecordingRepository;
import de.tobi6112.mm.inventoryservice.domain.repositories.ReleaseRepository;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

@ApplicationScoped
public class ArtistService {

  @Inject
  ReleaseRepository releaseRepository;

  @Inject
  ArtistRepository artistRepository;

  @Inject
  RecordingRepository recordingRepository;

  public Uni<ArtistDto> findByGid(UUID gid) {
    return Uni.createFrom().item(() -> artistRepository.findByGid(gid))
        .map(ArtistDto::fromArtist);
  }

  public Uni<List<AlbumDto>> findAllReleasesByArtistGid(UUID gid) {
    return Uni.createFrom().item(() -> releaseRepository.findAllReleasesByArtistGid(gid))
        .onItem().ifNull().failWith(new WebApplicationException("Artist Not Found", Status.NOT_FOUND))
        .onItem().ifNotNull().transform(releases -> releases.stream().map(AlbumDto::fromRelease).collect(
            Collectors.toList()));
  }

  public Uni<List<ArtistDto>> findAllByName(String name, Integer page, Integer limit) {
    return Uni.createFrom().item(() -> artistRepository.findAllByNameContainingIgnoreCase(name, Page.of(page, limit)))
        .onItem().ifNull().switchTo(() -> Uni.createFrom().item(Collections.emptyList()))
        .onItem().ifNotNull().transform(artist -> artist.stream().map(ArtistDto::fromArtist).collect(
            Collectors.toList()));
  }
}
