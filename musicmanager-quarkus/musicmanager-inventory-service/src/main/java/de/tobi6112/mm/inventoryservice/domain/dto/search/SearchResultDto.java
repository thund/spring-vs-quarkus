package de.tobi6112.mm.inventoryservice.domain.dto.search;

import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import io.quarkus.runtime.annotations.RegisterForReflection;
import java.util.List;
import java.util.Set;
import lombok.Value;

@RegisterForReflection
@Value
public class SearchResultDto {
  List<AlbumDto> albums;
  List<TrackDto> tracks;
  List<ArtistDto> artists;
}
