package de.tobi6112.mm.inventoryservice.domain.dto.artist;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.ArtistAlias;
import de.tobi6112.mm.inventoryservice.util.DateUtil;
import io.quarkus.runtime.annotations.RegisterForReflection;
import java.util.Optional;
import lombok.Value;

@RegisterForReflection
@Value
public class ArtistAliasDto {
  String name;
  String startDate;
  String endDate;
  String type;
  Boolean ended;

  public static ArtistAliasDto fromArtistAlias(ArtistAlias artistAlias) {
    var name = artistAlias.getName();
    var startDate = DateUtil.createDateStringFromFragments(artistAlias.getBeginDateYear(),
        artistAlias.getBeginDateMonth(), artistAlias.getBeginDateDay());
    var endDate = DateUtil.createDateStringFromFragments(artistAlias.getEndDateYear(),
        artistAlias.getEndDateMonth(), artistAlias.getEndDateDay());
    var type = Optional.ofNullable(artistAlias.getType()).map(t -> t.getName()).orElse(null);
    var ended = artistAlias.getEnded();
    return new ArtistAliasDto(name, startDate, endDate, type, ended);
  }
}
