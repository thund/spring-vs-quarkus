package de.tobi6112.mm.inventoryservice.domain.dto.album;

import de.tobi6112.mm.inventoryservice.domain.entities.release.Language;
import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Value;

@RegisterForReflection
@Value
public class LanguageDto {
  String isoCode3;
  String isoCode2;
  String name;
  Short frequency;

  public static LanguageDto fromLanguage(Language language) {
    var isoCode3 = language.getIsoCode3();
    var isoCode2 = language.getIsoCode1();
    var name = language.getName();
    var frequency = language.getFrequency();
    return new LanguageDto(isoCode3, isoCode2, name, frequency);
  }
}
