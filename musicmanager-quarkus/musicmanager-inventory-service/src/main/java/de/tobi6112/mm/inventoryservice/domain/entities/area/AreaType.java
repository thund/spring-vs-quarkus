package de.tobi6112.mm.inventoryservice.domain.entities.area;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "area_type")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class AreaType {
  @Id
  @Column(name = "id", nullable = false)
  private Integer id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "parent")
  private Integer parent;

  @Column(name = "child_order", nullable = false)
  private Integer childOrder;

  @Column(name = "description")
  private String description;

  @Column(name = "gid", unique = true, nullable = false)
  private UUID gid;

}
