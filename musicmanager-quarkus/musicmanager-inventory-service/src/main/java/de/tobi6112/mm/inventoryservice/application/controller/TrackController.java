package de.tobi6112.mm.inventoryservice.application.controller;

import de.tobi6112.mm.inventoryservice.application.service.TrackService;
import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("tracks")
public class TrackController {
  @Inject
  TrackService trackService;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  public Uni<TrackDto> getTrackByGid(@PathParam("id") UUID id) {
    return trackService.getTrackByGid(id);
  }

  @HEAD
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  public Uni<Response> existsById(@PathParam("id") UUID id) {
    return trackService.existsTrackByGid(id)
        .map(exists -> {
          if(exists) {
            return Response.ok().build();
          }
          return Response.status(Status.NOT_FOUND).build();
        });
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/artists")
  public Uni<List<ArtistDto>> getArtistsOfTrackByGid(@PathParam("id") UUID id) {
    return trackService.getArtistsOfTrackByGid(id);
  }

  @HEAD
  @Produces(MediaType.APPLICATION_JSON)
  @Path("checkTracks")
  public Uni<Response> existsTrackByGid(@QueryParam("id") List<UUID> id) {
    return trackService.existsTrackByGid(id)
        .map(res -> res ? Response.ok().build() : Response.status(Status.NOT_FOUND).build());
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("search")
  public Uni<List<TrackDto>> searchByName(@QueryParam("q") String query,
      @QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("limit") @DefaultValue("25") Integer limit) {
    return trackService.findAllByName(query, page, limit);
  }
}
