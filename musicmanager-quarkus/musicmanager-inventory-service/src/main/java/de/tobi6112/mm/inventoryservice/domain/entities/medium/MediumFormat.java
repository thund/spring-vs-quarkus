package de.tobi6112.mm.inventoryservice.domain.entities.medium;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "medium_format")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class MediumFormat {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "parent")
    private Integer parent;

    @Column(name = "child_order")
    private Integer childOrder;

    @Column(name = "year")
    private Short year;

    @Column(name = "has_discids")
    private Boolean hasDiscids;

    @Column(name = "description")
    private String description;

    @Column(name = "gid", unique = true)
    private UUID gid;

}
