package de.tobi6112.mm.inventoryservice.domain.entities.release;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "language")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class Language {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "iso_code_2t", unique = true, columnDefinition = "bpchar")
    private String isoCode2T;

    @Column(name = "iso_code_2b", unique = true, columnDefinition = "bpchar")
    private String isoCode2B;

    @Column(name = "iso_code_1", unique = true, columnDefinition = "bpchar")
    private String isoCode1;

    @Column(name = "name")
    private String name;

    @Column(name = "frequency")
    private Short frequency;

    @Column(name = "iso_code_3", unique = true, columnDefinition = "bpchar")
    private String isoCode3;

}
