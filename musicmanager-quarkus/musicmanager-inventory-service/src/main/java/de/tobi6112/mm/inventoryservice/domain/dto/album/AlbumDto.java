package de.tobi6112.mm.inventoryservice.domain.dto.album;

import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import io.quarkus.runtime.annotations.RegisterForReflection;
import java.util.Optional;
import java.util.UUID;
import lombok.Value;

@RegisterForReflection
@Value
public class AlbumDto {
  UUID gid;
  String name;
  String status;
  ReleaseGroupDto releaseGroup;
  LanguageDto language;

  public static AlbumDto fromRelease(Release release) {
    var gid = release.getGid();
    var name = release.getName();
    var status = Optional.ofNullable(release.getStatus()).map(s -> s.getName()).orElse(null);
    var releaseGroup = Optional.ofNullable(release.getReleaseGroup()).map(r -> ReleaseGroupDto.fromReleaseGroup(r)).orElse(null);
    var language = Optional.ofNullable(release.getLanguage()).map(l -> LanguageDto.fromLanguage(l)).orElse(null);

    return new AlbumDto(gid, name, status, releaseGroup,language);
  }
}
