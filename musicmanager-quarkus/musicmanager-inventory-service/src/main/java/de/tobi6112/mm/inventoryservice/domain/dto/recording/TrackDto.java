package de.tobi6112.mm.inventoryservice.domain.dto.recording;

import de.tobi6112.mm.inventoryservice.domain.entities.recording.Recording;
import io.quarkus.runtime.annotations.RegisterForReflection;
import java.util.UUID;
import lombok.Value;

@RegisterForReflection
@Value
public class TrackDto {
  UUID gid;
  String name;
  String artistCredit;
  Integer length;
  String comment;

  public static TrackDto fromRecording(Recording recording) {
    var gid = recording.getGid();
    var name = recording.getName();
    var artistCredit = recording.getArtistCredit().getName();
    var length = recording.getLength();
    var comment = recording.getComment();

    return new TrackDto(gid, name, artistCredit, length, comment);
  }
}
