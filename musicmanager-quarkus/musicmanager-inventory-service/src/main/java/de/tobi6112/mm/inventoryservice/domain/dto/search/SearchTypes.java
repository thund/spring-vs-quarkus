package de.tobi6112.mm.inventoryservice.domain.dto.search;

public enum SearchTypes {
  ALBUM, TRACK, ARTIST
}
