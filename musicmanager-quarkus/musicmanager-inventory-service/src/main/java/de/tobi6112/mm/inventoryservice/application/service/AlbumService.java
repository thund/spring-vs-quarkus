package de.tobi6112.mm.inventoryservice.application.service;

import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import de.tobi6112.mm.inventoryservice.domain.repositories.ArtistRepository;
import de.tobi6112.mm.inventoryservice.domain.repositories.RecordingRepository;
import de.tobi6112.mm.inventoryservice.domain.repositories.ReleaseRepository;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

@ApplicationScoped
public class AlbumService {

  @Inject
  ReleaseRepository releaseRepository;

  @Inject
  ArtistRepository artistRepository;

  @Inject
  RecordingRepository recordingRepository;

  public Uni<AlbumDto> findAlbumByGid(UUID gid) {
    return Uni.createFrom().item(() -> releaseRepository.findByGid(gid))
        .map(AlbumDto::fromRelease);
  }

  public Uni<List<ArtistDto>> findArtistsOfAlbumByGid(UUID gid) {
    return Uni.createFrom().item(() ->artistRepository.findAllArtistsOfRelease(gid))
        .onItem().ifNull().failWith(new WebApplicationException("Album Not Found", Status.NOT_FOUND))
        .onItem().ifNotNull().transform(artists -> artists.stream().map(ArtistDto::fromArtist).collect(
            Collectors.toList()));
  }

  public Uni<List<TrackDto>> findTracksOfAlbumByGid(UUID gid) {
    return Uni.createFrom().item(() -> recordingRepository.findAllRecordingsOfRelease(gid))
        .onItem().ifNull().failWith(new WebApplicationException("Album Not Found", Status.NOT_FOUND))
        .onItem().ifNotNull().transform(recordings -> recordings.stream().map(TrackDto::fromRecording).collect(
            Collectors.toList()));
  }

  public Uni<List<AlbumDto>> findAllByName(String name, Integer page, Integer limit) {
    return Uni.createFrom().item(() -> releaseRepository.findAllByNameContainingIgnoreCase(name, Page.of(page, limit)))
        .onItem().ifNull().switchTo(() -> Uni.createFrom().item(Collections.emptyList()))
        .onItem().ifNotNull().transform(album -> album.stream().map(AlbumDto::fromRelease).collect(
            Collectors.toList()));
  }
}
