package de.tobi6112.mm.inventoryservice.domain.entities.artist;

import de.tobi6112.mm.inventoryservice.domain.entities.area.Area;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "artist")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class Artist {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "gid", unique = true, nullable = false)
    private UUID gid;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sort_name", nullable = false)
    private String sortName;

    @Column(name = "begin_date_year")
    private Short beginDateYear;

    @Column(name = "begin_date_month")
    private Short beginDateMonth;

    @Column(name = "begin_date_day")
    private Short beginDateDay;

    @Column(name = "end_date_year")
    private Short endDateYear;

    @Column(name = "end_date_month")
    private Short endDateMonth;

    @Column(name = "end_date_day")
    private Short endDateDay;

    @JoinColumn(name = "type", referencedColumnName = "id")
    @ManyToOne
    private ArtistType type;

    @JoinColumn(name = "area", referencedColumnName = "id")
    @ManyToOne
    private Area area;

    @JoinColumn(name = "gender", referencedColumnName = "id")
    @ManyToOne
    private Gender gender;

    @Column(name = "comment", nullable = false)
    private String comment;

    @Column(name = "edits_pending", nullable = false)
    private Integer editsPending;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @Column(name = "ended", nullable = false)
    private Boolean ended;

    @Column(name = "begin_area")
    private Integer beginArea;

    @Column(name = "end_area")
    private Integer endArea;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumns({
        @JoinColumn(name = "artist", referencedColumnName = "id")
    })
    private List<ArtistAlias> aliases;

}
