package de.tobi6112.mm.inventoryservice.application.controller;

import de.tobi6112.mm.inventoryservice.application.service.AlbumService;
import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import de.tobi6112.mm.inventoryservice.domain.dto.recording.TrackDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("albums")
public class AlbumController {
  @Inject
  AlbumService albumService;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  public Uni<AlbumDto> getAlbumById(@PathParam("id") UUID id) {
    return albumService.findAlbumByGid(id);
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/artists")
  public Uni<List<ArtistDto>> getArtistsOfAlbumByGid(@PathParam("id") UUID id) {
    return albumService.findArtistsOfAlbumByGid(id);
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/tracks")
  public Uni<List<TrackDto>> getTracksOfAlbumByGid(@PathParam("id") UUID id) {
    return albumService.findTracksOfAlbumByGid(id);
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("search")
  public Uni<List<AlbumDto>> searchByName(@QueryParam("q") String query,
      @QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("limit") @DefaultValue("25") Integer limit) {
    return albumService.findAllByName(query, page, limit);
  }
}
