package de.tobi6112.mm.inventoryservice.domain.repositories;

import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ReleaseRepository implements PanacheRepositoryBase<Release, Integer> {
  public Release findByGid(UUID gid) {
    return find("gid", gid).firstResult();
  }

  public List<Release> findAllByNameContainingIgnoreCase(String name, Page page) {
    return find("lower(name) like concat('%', lower(?1), '%')", name)
        .page(page)
        .list();
  }

  public List<Release> findAllReleasesByArtistGid(UUID id) {
    return find("select r from Release r join r.releaseGroup rg join rg.artistCredit ac join ac.artistCreditNames acn join acn.artist a where a.gid = ?1", id)
        .list();
  }
}
