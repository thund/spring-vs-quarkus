package de.tobi6112.mm.inventoryservice.application.controller;

import de.tobi6112.mm.inventoryservice.application.service.ArtistService;
import de.tobi6112.mm.inventoryservice.domain.dto.album.AlbumDto;
import de.tobi6112.mm.inventoryservice.domain.dto.artist.ArtistDto;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("artists")
public class ArtistController {
  @Inject
  ArtistService artistService;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  public Uni<ArtistDto> getArtistById(@PathParam("id") UUID id) {
    return this.artistService.findByGid(id);
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}/albums")
  public Uni<List<AlbumDto>> getAlbumsOfArtistById(@PathParam("id") UUID id) {
    return artistService.findAllReleasesByArtistGid(id);
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("search")
  public Uni<List<ArtistDto>> searchByName(@QueryParam("q") String query,
      @QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("limit") @DefaultValue("25") Integer limit) {
    return artistService.findAllByName(query, page, limit);
  }
}
