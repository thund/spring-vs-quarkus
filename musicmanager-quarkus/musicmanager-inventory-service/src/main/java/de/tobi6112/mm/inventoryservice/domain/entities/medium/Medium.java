package de.tobi6112.mm.inventoryservice.domain.entities.medium;

import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "medium")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class Medium {
    @Id
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "release", referencedColumnName = "id")
    @ManyToOne
    private Release release;

    @Column(name = "position")
    private Integer position;

    @JoinColumn(name = "format", referencedColumnName = "id")
    @ManyToOne
    private MediumFormat format;

    @Column(name = "name")
    private String name;

    @Column(name = "edits_pending")
    private Integer editsPending;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @Column(name = "track_count")
    private Integer trackCount;

}
