package de.tobi6112.mm.inventoryservice.domain.repositories;

import de.tobi6112.mm.inventoryservice.domain.entities.artist.Artist;
import de.tobi6112.mm.inventoryservice.domain.entities.release.Release;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ArtistRepository implements PanacheRepositoryBase<Artist, Integer> {
  public Artist findByGid(UUID gid) {
    return find("gid", gid).firstResult();
  }

  public List<Artist> findAllByNameContainingIgnoreCase(String query, Page page) {
    return find("select a from Artist a where lower(a.name) like concat('%', lower(?1), '%')", query)
        .page(page)
        .list();
  }

  public List<Artist> findAllArtistsOfRecording(UUID gid) {
    return find("select a from Recording rec join rec.artistCredit ac join ac.artistCreditNames acn join acn.artist a where rec.gid = ?1", gid)
        .list();
  }

  public List<Artist> findAllArtistsOfRelease(UUID gid) {
    return find("select a from Release r join r.artistCredit ac join ac.artistCreditNames acn join acn.artist a where r.gid = ?1", gid)
        .list();
  }
}
