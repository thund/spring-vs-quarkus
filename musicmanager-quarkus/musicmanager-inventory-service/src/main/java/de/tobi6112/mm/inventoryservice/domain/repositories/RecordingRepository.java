package de.tobi6112.mm.inventoryservice.domain.repositories;

import de.tobi6112.mm.inventoryservice.domain.entities.recording.Recording;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.quarkus.panache.common.Page;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RecordingRepository implements PanacheRepositoryBase<Recording, Integer> {
  public Recording findByGid(UUID gid) {
    return find("gid", gid).firstResult();
  }

  public Boolean existsByGid(UUID gid) {
    return count("gid", gid) > 0;
  }

  public List<Recording> findAllByNameContainingIgnoreCase(String name, Page page) {
    return find("lower(name) like concat('%', lower(?1), '%')", name)
        .page(page)
        .list();
  }

  public List<Recording> findAllRecordingsOfRelease(UUID id) {
    return find("select rec from Track t join t.recording rec join t.medium m join m.release r where r.gid = ?1", id)
        .list();
  }

  public Boolean existsTrackByGid(List<UUID> gid) {
    return list("gid in ?1", gid).size() == gid.size();
  }
}
