CREATE EXTENSION cube;
CREATE EXTENSION earthdistance;

CREATE SCHEMA musicbrainz;
CREATE SCHEMA statistics;
CREATE SCHEMA cover_art_archive;
CREATE SCHEMA wikidocs;
CREATE SCHEMA documentation;