BEGIN;
drop table if exists  alternative_release CASCADE;
drop table if exists  alternative_release_type CASCADE;
drop table if exists  alternative_medium CASCADE;
drop table if exists  alternative_track CASCADE;
drop table if exists  alternative_medium_track CASCADE;
drop table if exists  annotation CASCADE;
drop table if exists  application CASCADE;
drop table if exists  area_gid_redirect CASCADE;
drop table if exists  area_alias_type CASCADE;
drop table if exists  area_alias CASCADE;
drop table if exists  area_annotation CASCADE;
drop table if exists  area_attribute_type CASCADE;
drop table if exists  area_attribute_type_allowed_value CASCADE;
drop table if exists  area_attribute CASCADE;
drop table if exists  area_tag CASCADE;
drop table if exists  area_tag_raw CASCADE;
drop table if exists  artist_annotation CASCADE;
drop table if exists  artist_attribute_type CASCADE;
drop table if exists  artist_attribute_type_allowed_value CASCADE;
drop table if exists  artist_attribute CASCADE;
drop table if exists  artist_ipi CASCADE;
drop table if exists  artist_isni CASCADE;
drop table if exists  artist_meta CASCADE;
drop table if exists  artist_tag CASCADE;
drop table if exists  artist_rating_raw CASCADE;
drop table if exists  artist_tag_raw CASCADE;
drop table if exists  artist_gid_redirect CASCADE;
drop table if exists  autoeditor_election CASCADE;
drop table if exists  autoeditor_election_vote CASCADE;
drop table if exists  cdtoc CASCADE;
drop table if exists  cdtoc_raw CASCADE;
drop table if exists  country_area CASCADE;
drop table if exists  deleted_entity CASCADE;
drop table if exists  edit CASCADE;
drop table if exists  edit_data CASCADE;
drop table if exists  edit_note CASCADE;
drop table if exists  edit_note_recipient CASCADE;
drop table if exists  edit_area CASCADE;
drop table if exists  edit_artist CASCADE;
drop table if exists  edit_event CASCADE;
drop table if exists  edit_instrument CASCADE;
drop table if exists  edit_label CASCADE;
drop table if exists  edit_place CASCADE;
drop table if exists  edit_release CASCADE;
drop table if exists  edit_release_group CASCADE;
drop table if exists  edit_recording CASCADE;
drop table if exists  edit_series CASCADE;
drop table if exists  edit_work CASCADE;
drop table if exists  edit_url CASCADE;
drop table if exists  editor CASCADE;
drop table if exists  old_editor_name CASCADE;
drop table if exists  editor_language CASCADE;
drop table if exists  editor_preference CASCADE;
drop table if exists  editor_subscribe_artist CASCADE;
drop table if exists  editor_subscribe_artist_deleted CASCADE;
drop table if exists  editor_subscribe_collection CASCADE;
drop table if exists  editor_subscribe_label CASCADE;
drop table if exists  editor_subscribe_label_deleted CASCADE;
drop table if exists  editor_subscribe_editor CASCADE;
drop table if exists  editor_subscribe_series CASCADE;
drop table if exists  editor_subscribe_series_deleted CASCADE;
drop table if exists  event CASCADE;
drop table if exists  event_meta CASCADE;
drop table if exists  event_rating_raw CASCADE;
drop table if exists  event_tag_raw CASCADE;
drop table if exists  event_alias_type CASCADE;
drop table if exists  event_alias CASCADE;
drop table if exists  event_annotation CASCADE;
drop table if exists  event_attribute_type CASCADE;
drop table if exists  event_attribute_type_allowed_value CASCADE;
drop table if exists  event_attribute CASCADE;
drop table if exists  event_gid_redirect CASCADE;
drop table if exists  event_tag CASCADE;
drop table if exists  event_type CASCADE;
drop table if exists  genre CASCADE;
drop table if exists  genre_alias CASCADE;
drop table if exists  instrument_type CASCADE;
drop table if exists  instrument CASCADE;
drop table if exists  instrument_gid_redirect CASCADE;
drop table if exists  instrument_alias_type CASCADE;
drop table if exists  instrument_alias CASCADE;
drop table if exists  instrument_annotation CASCADE;
drop table if exists  instrument_attribute_type CASCADE;
drop table if exists  instrument_attribute_type_allowed_value CASCADE;
drop table if exists  instrument_attribute CASCADE;
drop table if exists  instrument_tag CASCADE;
drop table if exists  instrument_tag_raw CASCADE;
drop table if exists  iso_3166_1 CASCADE;
drop table if exists  iso_3166_2 CASCADE;
drop table if exists  iso_3166_3 CASCADE;
drop table if exists  isrc CASCADE;
drop table if exists  iswc CASCADE;
drop table if exists  l_area_area CASCADE;
drop table if exists  l_area_artist CASCADE;
drop table if exists  l_area_event CASCADE;
drop table if exists  l_area_instrument CASCADE;
drop table if exists  l_area_label CASCADE;
drop table if exists  l_area_place CASCADE;
drop table if exists  l_area_recording CASCADE;
drop table if exists  l_area_release CASCADE;
drop table if exists  l_area_release_group CASCADE;
drop table if exists  l_area_series CASCADE;
drop table if exists  l_area_url CASCADE;
drop table if exists  l_area_work CASCADE;
drop table if exists  l_artist_artist CASCADE;
drop table if exists  l_artist_event CASCADE;
drop table if exists  l_artist_instrument CASCADE;
drop table if exists  l_artist_label CASCADE;
drop table if exists  l_artist_place CASCADE;
drop table if exists  l_artist_recording CASCADE;
drop table if exists  l_artist_release CASCADE;
drop table if exists  l_artist_release_group CASCADE;
drop table if exists  l_artist_series CASCADE;
drop table if exists  l_artist_url CASCADE;
drop table if exists  l_artist_work CASCADE;
drop table if exists  l_event_event CASCADE;
drop table if exists  l_event_instrument CASCADE;
drop table if exists  l_event_label CASCADE;
drop table if exists  l_event_place CASCADE;
drop table if exists  l_event_recording CASCADE;
drop table if exists  l_event_release CASCADE;
drop table if exists  l_event_release_group CASCADE;
drop table if exists  l_event_series CASCADE;
drop table if exists  l_event_url CASCADE;
drop table if exists  l_event_work CASCADE;
drop table if exists  l_label_label CASCADE;
drop table if exists  l_instrument_instrument CASCADE;
drop table if exists  l_instrument_label CASCADE;
drop table if exists  l_instrument_place CASCADE;
drop table if exists  l_instrument_recording CASCADE;
drop table if exists  l_instrument_release CASCADE;
drop table if exists  l_instrument_release_group CASCADE;
drop table if exists  l_instrument_series CASCADE;
drop table if exists  l_instrument_url CASCADE;
drop table if exists  l_instrument_work CASCADE;
drop table if exists  l_label_place CASCADE;
drop table if exists  l_label_recording CASCADE;
drop table if exists  l_label_release CASCADE;
drop table if exists  l_label_release_group CASCADE;
drop table if exists  l_label_series CASCADE;
drop table if exists  l_label_url CASCADE;
drop table if exists  l_label_work CASCADE;
drop table if exists  l_place_place CASCADE;
drop table if exists  l_place_recording CASCADE;
drop table if exists  l_place_release CASCADE;
drop table if exists  l_place_release_group CASCADE;
drop table if exists  l_place_series CASCADE;
drop table if exists  l_place_url CASCADE;
drop table if exists  l_place_work CASCADE;
drop table if exists  l_recording_recording CASCADE;
drop table if exists  l_recording_release CASCADE;
drop table if exists  l_recording_release_group CASCADE;
drop table if exists  l_recording_series CASCADE;
drop table if exists  l_recording_url CASCADE;
drop table if exists  l_recording_work CASCADE;
drop table if exists  l_release_release CASCADE;
drop table if exists  l_release_release_group CASCADE;
drop table if exists  l_release_series CASCADE;
drop table if exists  l_release_url CASCADE;
drop table if exists  l_release_work CASCADE;
drop table if exists  l_release_group_release_group CASCADE;
drop table if exists  l_release_group_series CASCADE;
drop table if exists  l_release_group_url CASCADE;
drop table if exists  l_release_group_work CASCADE;
drop table if exists  l_series_series CASCADE;
drop table if exists  l_series_url CASCADE;
drop table if exists  l_series_work CASCADE;
drop table if exists  l_url_url CASCADE;
drop table if exists  l_url_work CASCADE;
drop table if exists  l_work_work CASCADE;
drop table if exists  label CASCADE;
drop table if exists  label_rating_raw CASCADE;
drop table if exists  label_tag_raw CASCADE;
drop table if exists  label_alias_type CASCADE;
drop table if exists  label_alias CASCADE;
drop table if exists  label_annotation CASCADE;
drop table if exists  label_attribute_type CASCADE;

drop table if exists  label_attribute_type_allowed_value CASCADE;
drop table if exists  label_attribute CASCADE;
drop table if exists  label_ipi CASCADE;
drop table if exists  label_isni CASCADE;
drop table if exists  label_meta CASCADE;
drop table if exists  label_gid_redirect CASCADE;
drop table if exists  label_tag CASCADE;
drop table if exists  label_type CASCADE;
drop table if exists  link CASCADE;
drop table if exists  link_attribute CASCADE;
drop table if exists  link_attribute_type CASCADE;
drop table if exists  link_creditable_attribute_type CASCADE;
drop table if exists  link_attribute_credit CASCADE;
drop table if exists  link_text_attribute_type CASCADE;
drop table if exists  link_attribute_text_value CASCADE;
drop table if exists  link_type CASCADE;
drop table if exists  link_type_attribute_type CASCADE;
drop table if exists  editor_collection CASCADE;
drop table if exists  editor_collection_type CASCADE;
drop table if exists  editor_collection_collaborator CASCADE;
drop table if exists  editor_collection_area CASCADE;
drop table if exists  editor_collection_artist CASCADE;
drop table if exists  editor_collection_event CASCADE;
drop table if exists  editor_collection_instrument CASCADE;
drop table if exists  editor_collection_label CASCADE;
drop table if exists  editor_collection_place CASCADE;
drop table if exists  editor_collection_recording CASCADE;
drop table if exists  editor_collection_release CASCADE;
drop table if exists  editor_collection_release_group CASCADE;
drop table if exists  editor_collection_series CASCADE;
drop table if exists  editor_collection_work CASCADE;
drop table if exists  editor_collection_deleted_entity CASCADE;
drop table if exists  editor_oauth_token CASCADE;
drop table if exists  editor_watch_preferences CASCADE;
drop table if exists  editor_watch_artist CASCADE;
drop table if exists  editor_watch_release_group_type CASCADE;
drop table if exists  editor_watch_release_status CASCADE;
drop table if exists  medium_attribute_type CASCADE;
drop table if exists  medium_attribute_type_allowed_format CASCADE;
drop table if exists  medium_attribute_type_allowed_value CASCADE;
drop table if exists  medium_attribute_type_allowed_value_allowed_format CASCADE;
drop table if exists  medium_attribute CASCADE;
drop table if exists  medium_cdtoc CASCADE;
drop table if exists  orderable_link_type CASCADE;
drop table if exists  place CASCADE;
drop table if exists  place_alias CASCADE;
drop table if exists  place_alias_type CASCADE;
drop table if exists  place_annotation CASCADE;
drop table if exists  place_attribute_type CASCADE;
drop table if exists  place_attribute_type_allowed_value CASCADE;
drop table if exists  place_attribute CASCADE;
drop table if exists  place_gid_redirect CASCADE;
drop table if exists  place_tag CASCADE;
drop table if exists  place_tag_raw CASCADE;
drop table if exists  place_type CASCADE;
drop table if exists  replication_control CASCADE;
drop table if exists  recording_alias_type CASCADE;
drop table if exists  recording_alias CASCADE;
drop table if exists  recording_rating_raw CASCADE;
drop table if exists  recording_tag_raw CASCADE;
drop table if exists  recording_annotation CASCADE;
drop table if exists  recording_attribute_type CASCADE;
drop table if exists  recording_attribute_type_allowed_value CASCADE;
drop table if exists  recording_attribute CASCADE;
drop table if exists  recording_meta CASCADE;
drop table if exists  recording_gid_redirect CASCADE;
drop table if exists  recording_tag CASCADE;
drop table if exists  release_alias_type CASCADE;
drop table if exists  release_alias CASCADE;
drop table if exists  release_country CASCADE;
drop table if exists  release_unknown_country CASCADE;
drop table if exists  release_raw CASCADE;
drop table if exists  release_tag_raw CASCADE;
drop table if exists  release_annotation CASCADE;
drop table if exists  release_attribute_type CASCADE;
drop table if exists  release_attribute_type_allowed_value CASCADE;
drop table if exists  release_attribute CASCADE;
drop table if exists  release_gid_redirect CASCADE;
drop table if exists  release_meta CASCADE;
drop table if exists  release_coverart CASCADE;
drop table if exists  release_label CASCADE;
drop table if exists  release_tag CASCADE;
drop table if exists  release_group_alias_type CASCADE;
drop table if exists  release_group_alias CASCADE;
drop table if exists  release_group_rating_raw CASCADE;
drop table if exists  release_group_tag_raw CASCADE;
drop table if exists  release_group_annotation CASCADE;
drop table if exists  release_group_attribute_type CASCADE;
drop table if exists  release_group_attribute_type_allowed_value CASCADE;
drop table if exists  release_group_attribute CASCADE;
drop table if exists  release_group_gid_redirect CASCADE;
drop table if exists  release_group_meta CASCADE;
drop table if exists  release_group_tag CASCADE;
drop table if exists  release_group_secondary_type CASCADE;
drop table if exists  release_group_secondary_type_join CASCADE;
drop table if exists  series CASCADE;
drop table if exists  series_type CASCADE;
drop table if exists  series_ordering_type CASCADE;
drop table if exists  series_gid_redirect CASCADE;
drop table if exists  series_alias_type CASCADE;
drop table if exists  series_alias CASCADE;
drop table if exists  series_annotation CASCADE;
drop table if exists  series_attribute_type CASCADE;
drop table if exists  series_attribute_type_allowed_value CASCADE;
drop table if exists  series_attribute CASCADE;
drop table if exists  series_tag CASCADE;
drop table if exists  series_tag_raw CASCADE;
drop table if exists  tag CASCADE;
drop table if exists  tag_relation CASCADE;
drop table if exists  track_gid_redirect CASCADE;
drop table if exists  track_raw CASCADE;
drop table if exists  medium_index CASCADE;
drop table if exists  url CASCADE;
drop table if exists  url_gid_redirect CASCADE;
drop table if exists  vote CASCADE;
drop table if exists  work CASCADE;
drop table if exists  work_language CASCADE;
drop table if exists  work_rating_raw CASCADE;
drop table if exists  work_tag_raw CASCADE;
drop table if exists  work_alias_type CASCADE;
drop table if exists  work_alias CASCADE;
drop table if exists  work_annotation CASCADE;
drop table if exists  work_gid_redirect CASCADE;
drop table if exists  work_meta CASCADE;
drop table if exists  work_tag CASCADE;
drop table if exists  work_type CASCADE;
drop table if exists  work_attribute_type CASCADE;
drop table if exists  work_attribute_type_allowed_value CASCADE;
drop table if exists  work_attribute CASCADE;
drop table if exists artist_release_group_pending_update CASCADE;
drop table if exists artist_release_pending_update CASCADE;
drop table if exists editor_collection_gid_redirect CASCADE;
drop table if exists place_meta CASCADE;
drop table if exists place_rating_raw CASCADE;
drop table if exists recording_first_release_date CASCADE;
drop table if exists release_first_release_date CASCADE;
drop table if exists artist_release CASCADE;
drop table if exists artist_release_group CASCADE;




-------------------------------------------------------------------

ALTER TABLE area          ADD CHECK (controlled_for_whitespace(comment));
ALTER TABLE artist        ADD CHECK (controlled_for_whitespace(comment));
ALTER TABLE medium        ADD CHECK (controlled_for_whitespace(name));
ALTER TABLE recording     ADD CHECK (controlled_for_whitespace(comment));
ALTER TABLE release       ADD CHECK (controlled_for_whitespace(comment));
ALTER TABLE release_group ADD CHECK (controlled_for_whitespace(comment));
ALTER TABLE track         ADD CHECK (controlled_for_whitespace(number));

ALTER TABLE area
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != '');

ALTER TABLE artist
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != ''),
  ADD CONSTRAINT control_for_whitespace_sort_name CHECK (controlled_for_whitespace(sort_name)),
  ADD CONSTRAINT only_non_empty_sort_name CHECK (sort_name != '');

ALTER TABLE artist_alias
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != ''),
  ADD CONSTRAINT control_for_whitespace_sort_name CHECK (controlled_for_whitespace(sort_name)),
  ADD CONSTRAINT only_non_empty_sort_name CHECK (sort_name != '');

ALTER TABLE artist_credit
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != '');

ALTER TABLE artist_credit_name
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != '');

DELETE FROM release WHERE controlled_for_whitespace(name) = false;

ALTER TABLE release
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != '');

DELETE FROM release_group WHERE controlled_for_whitespace(name) = false;

ALTER TABLE release_group
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != '');

DELETE FROM track WHERE controlled_for_whitespace(name) = false;

ALTER TABLE track
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != '');

DELETE FROM recording WHERE controlled_for_whitespace(name) = false;

ALTER TABLE recording
    ADD CONSTRAINT control_for_whitespace CHECK (controlled_for_whitespace(name)),
  ADD CONSTRAINT only_non_empty CHECK (name != '');

ALTER TABLE artist
    ADD CONSTRAINT group_type_implies_null_gender CHECK (
            (gender IS NULL AND type IN (2, 5, 6))
            OR type NOT IN (2, 5, 6)
            OR type IS NULL
        );

ALTER TABLE artist ADD CONSTRAINT artist_va_check
    CHECK (id <> 1 OR
           (type = 3 AND
            gender IS NULL AND
            area IS NULL AND
            begin_area IS NULL AND
            end_area IS NULL AND
            begin_date_year IS NULL AND
            begin_date_month IS NULL AND
            begin_date_day IS NULL AND
            end_date_year IS NULL AND
            end_date_month IS NULL AND
            end_date_day IS NULL));

ALTER TABLE medium ADD CONSTRAINT medium_uniq
    UNIQUE (release, position) DEFERRABLE INITIALLY IMMEDIATE;

ALTER TABLE track ADD CONSTRAINT track_uniq_medium_position
    UNIQUE (medium, position) DEFERRABLE INITIALLY IMMEDIATE;

---

ALTER TABLE area
    ADD CONSTRAINT area_fk_type
        FOREIGN KEY (type)
            REFERENCES area_type(id);

ALTER TABLE area_type
    ADD CONSTRAINT area_type_fk_parent
        FOREIGN KEY (parent)
            REFERENCES area_type(id);

ALTER TABLE artist
    ADD CONSTRAINT artist_fk_type
        FOREIGN KEY (type)
            REFERENCES artist_type(id);

ALTER TABLE artist
    ADD CONSTRAINT artist_fk_area
        FOREIGN KEY (area)
            REFERENCES area(id);

ALTER TABLE artist
    ADD CONSTRAINT artist_fk_gender
        FOREIGN KEY (gender)
            REFERENCES gender(id);

ALTER TABLE artist
    ADD CONSTRAINT artist_fk_begin_area
        FOREIGN KEY (begin_area)
            REFERENCES area(id);

ALTER TABLE artist
    ADD CONSTRAINT artist_fk_end_area
        FOREIGN KEY (end_area)
            REFERENCES area(id);

ALTER TABLE artist_alias
    ADD CONSTRAINT artist_alias_fk_artist
        FOREIGN KEY (artist)
            REFERENCES artist(id);

ALTER TABLE artist_alias
    ADD CONSTRAINT artist_alias_fk_type
        FOREIGN KEY (type)
            REFERENCES artist_alias_type(id);

ALTER TABLE artist_alias_type
    ADD CONSTRAINT artist_alias_type_fk_parent
        FOREIGN KEY (parent)
            REFERENCES artist_alias_type(id);

ALTER TABLE artist_credit_name
    ADD CONSTRAINT artist_credit_name_fk_artist_credit
        FOREIGN KEY (artist_credit)
            REFERENCES artist_credit(id)
            ON DELETE CASCADE;

ALTER TABLE artist_credit_name
    ADD CONSTRAINT artist_credit_name_fk_artist
        FOREIGN KEY (artist)
            REFERENCES artist(id)
            ON DELETE CASCADE;

ALTER TABLE artist_type
    ADD CONSTRAINT artist_type_fk_parent
        FOREIGN KEY (parent)
            REFERENCES artist_type(id);

ALTER TABLE gender
    ADD CONSTRAINT gender_fk_parent
        FOREIGN KEY (parent)
            REFERENCES gender(id);

DELETE FROM medium where release = 535113;
DELETE FROM medium where release = 779592;

ALTER TABLE medium
    ADD CONSTRAINT medium_fk_release
        FOREIGN KEY (release)
            REFERENCES release(id);

ALTER TABLE medium
    ADD CONSTRAINT medium_fk_format
        FOREIGN KEY (format)
            REFERENCES medium_format(id);

ALTER TABLE medium_format
    ADD CONSTRAINT medium_format_fk_parent
        FOREIGN KEY (parent)
            REFERENCES medium_format(id);

ALTER TABLE recording
    ADD CONSTRAINT recording_fk_artist_credit
        FOREIGN KEY (artist_credit)
            REFERENCES artist_credit(id);

ALTER TABLE release
    ADD CONSTRAINT release_fk_artist_credit
        FOREIGN KEY (artist_credit)
            REFERENCES artist_credit(id);

DELETE FROM medium where release = 535114;
DELETE FROM release where release_group = 818950;

ALTER TABLE release
    ADD CONSTRAINT release_fk_release_group
        FOREIGN KEY (release_group)
            REFERENCES release_group(id);

ALTER TABLE release
    ADD CONSTRAINT release_fk_status
        FOREIGN KEY (status)
            REFERENCES release_status(id);

ALTER TABLE release
    ADD CONSTRAINT release_fk_packaging
        FOREIGN KEY (packaging)
            REFERENCES release_packaging(id);

ALTER TABLE release
    ADD CONSTRAINT release_fk_language
        FOREIGN KEY (language)
            REFERENCES language(id);

ALTER TABLE release
    ADD CONSTRAINT release_fk_script
        FOREIGN KEY (script)
            REFERENCES script(id);

ALTER TABLE release_group
    ADD CONSTRAINT release_group_fk_artist_credit
        FOREIGN KEY (artist_credit)
            REFERENCES artist_credit(id);

ALTER TABLE release_group
    ADD CONSTRAINT release_group_fk_type
        FOREIGN KEY (type)
            REFERENCES release_group_primary_type(id);

ALTER TABLE release_group_primary_type
    ADD CONSTRAINT release_group_primary_type_fk_parent
        FOREIGN KEY (parent)
            REFERENCES release_group_primary_type(id);

ALTER TABLE release_packaging
    ADD CONSTRAINT release_packaging_fk_parent
        FOREIGN KEY (parent)
            REFERENCES release_packaging(id);

ALTER TABLE release_status
    ADD CONSTRAINT release_status_fk_parent
        FOREIGN KEY (parent)
            REFERENCES release_status(id);

DELETE FROM track where recording = 828832;
DELETE FROM track where recording = 4622954;
DELETE FROM track where recording = 8747739;
DELETE FROM track where recording = 9558233;
DELETE FROM track where recording = 12832032;

ALTER TABLE track
    ADD CONSTRAINT track_fk_recording
        FOREIGN KEY (recording)
            REFERENCES recording(id);

DELETE FROM track where medium = 535113;
DELETE FROM track where medium = 779592;
DELETE FROM track where medium = 535114;

ALTER TABLE track
    ADD CONSTRAINT track_fk_medium
        FOREIGN KEY (medium)
            REFERENCES medium(id);

ALTER TABLE track
    ADD CONSTRAINT track_fk_artist_credit
        FOREIGN KEY (artist_credit)
            REFERENCES artist_credit(id);

COMMIT;