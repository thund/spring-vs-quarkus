# Spring vs. Quarkus

This repository is about a comparison in terms of resource usage as well as performance between
Spring and Quarkus using a more realistic use-case by simulating a music management application.

The application serves the following use-cases:

![Use Case Diagram](./documentation/res/diagrams/MusicManager_UseCase.png)

It is structured in two components which are designed to be used as distinct services.

![Component Diagram](./documentation/res/diagrams/MusicManager_Component.png)

- **Music Inventory**:
  This service is responsible for managing the inventory the management application has to offer.
  In this example the [MusicBrainz](https://musicbrainz.org/) database is used as source of the data
  and can be retrieved via several HTTP APIs.

- **User Management**:
  This service is responsible for managing user data. It allows users to register themselves and
  manages their music playlists. Whenever a song is being added into a playlist, the song is checked
  for existence by invoking an API call to the Inventory. 

## Structure of this repo

The repo root contains 4 folders.
- `benchmark` contains all necessary files for performing a benchmark analysis
- `documentation` contains all diagrams created for this project, necessary database setup scripts as well as an OpenAPI documentation of the APIs
- `musicmanager-quarkus` is the project root of the quarkus implementations
- `musicmanger-spring` is the project root of the spring implementations

## Setup

This whole project is ideally served on two machines.

One machine (it can also be a virtual machine) is responsible for running the application itself as 
well as a [cadvisor](https://github.com/google/cadvisor) instance to collect runtime metrics.

The other machine is responsible for running the benchmark tests and consuming/collecting the 
metrics. Therefore [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/) are used.
In order to collect the benchmark metrics from [k6](https://k6.io/), a [StatsD Exporter](https://github.com/prometheus/statsd_exporter) 
is also required.

The whole setup is divided into multiple Docker Compose files.

### Prerequisites

- Docker
- K6

### Setup MusicBrainz Database

Please follow the steps under [https://musicbrainz.org/doc/MusicBrainz_Database](https://musicbrainz.org/doc/MusicBrainz_Database) to
obtain and use the Database.

As an alternative you can run your own PostgreSQL Database and execute the scripts found under
`documentation/res/db/sql` in the alphabetical order. Please note that you will still need the 
database dump for step 3.

The Database should use the `musicbrainz` database with credentials `musicbrainz:musicbrainz`.

### Run cadvisor

In order to collect runtime metrics of containers and node run:

```shell
$ docker-compose -f ./benchmark/docker-compose.cadvisor.yml up -d
```

### Build & Run different implementations

#### Spring

```shell
$ ./musicmanager-spring/mvnw clean package
$ docker-compose -f ./musicmanager-spring/docker-compose.yml up
```

#### Spring Native

```shell
$ ./musicmanager-spring/build-native.sh
$ docker-compose -f ./musicmanager-spring/docker-compose.native.yml up
```

#### Quarkus

```shell
$ ./musicmanager-spring/mvnw clean package
$ docker-compose -f ./musicmanager-quarkus/docker-compose.yml up
```

#### Quarkus Native

```shell
$ ./musicmanager-quarkus/build-native.sh
$ docker-compose -f ./musicmanager-quarkus/docker-compose.native.yml up
```

### Run Prometheus, Grafana, StatsD exporter

```shell
$ docker-compose -f ./benchmark/docker-compose.yml up 
```

The Grafana dashborad is located under `./benchmark/docker/grafana/mm_dashboard.json`

### Adjust configuration

In order to collect metrics and run the tests some adjustments in the configuration is required.

Fill in the IP address of the machine which runs cadvisor and the application in the following places:

`./benchmark/docker/prometheus/prometheus.yml`
```yaml
scrape_configs:
  - job_name: cadvisor
    static_configs:
      - targets:
          - 192.168.178.93:8080 # <-- HERE
```

`./benchmark/tests/common.js`
```js
export const BASE_URL_INVENTORY = "http://192.168.178.93:8080/api/v1"; // <-- HERE
export const BASE_URL_USER = "http://192.168.178.93:8081/api/v1"; // <-- HERE
```

### Run Benchmarks

```shell
$ cd ./benchmark
$ ./run.sh
```