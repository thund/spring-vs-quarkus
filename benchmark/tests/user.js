import { BASE_URL_USER} from "./common.js";
import http from "k6/http";

export function createUser(user) {
  return http.post(`${BASE_URL_USER}/users`, JSON.stringify(user)
  , {
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    tags: {
      "name": "postUser"
    }
  })
}

export function getUserByUsername(username) {
  return http.get(`${BASE_URL_USER}/users/${username}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getUserByUsername"
    }
  });
}

export function getUserPlaylists(username) {
  return http.get(`${BASE_URL_USER}/users/${username}/playlists`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getUserPlaylists"
    }
  });
}

export function createUserPlaylist(username, playlist) {
  return http.post(`${BASE_URL_USER}/users/${username}/playlists`, JSON.stringify(playlist), {
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    tags: {
      "name": "postUserPlaylists"
    }
  });
}

export function editPlaylist(id, playlist) {
  return http.put(`${BASE_URL_USER}/playlists/${id}`, JSON.stringify(playlist), {
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    tags: {
      "name": "putPlaylistById"
    }
  });
}

export function getPlaylist(id) {
  return http.get(`${BASE_URL_USER}/playlists/${id}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getPlaylistById"
    }
  });
}

export function deletePlaylist(id) {
  return http.del(`${BASE_URL_USER}/playlists/${id}`, null, {
    tags: {
      "name": "deletePlaylistById"
    }
  });
}

export function getTracksOfPlaylist(id) {
  return http.get(`${BASE_URL_USER}/playlists/${id}/tracks`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getPlaylistTracks"
    }
  });
}

export function addTracksToPlaylist(id, playlistEntries) {
  return http.post(`${BASE_URL_USER}/playlists/${id}/tracks`, JSON.stringify(playlistEntries), {
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
    tags: {
      "name": "postPlaylistTracks"
    }
  });
}

export function removeTracksToPlaylist(id, trackIds) {
  return http.del(`${BASE_URL_USER}/playlists/${id}/tracks?id=${trackIds.join('&id=')}`, null, {
    tags: {
      "name": "deletePlaylistEntries"
    }
  });
}

export function searchPlaylist(q) {
  return http.get(`${BASE_URL_USER}/playlists/search?q=${q}`,  {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "searchPlaylist"
    }
  })
}