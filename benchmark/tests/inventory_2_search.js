import {SharedArray} from "k6/data";
import {
  findAlbumsByName,
  findArtistsByName,
  findTracksByName
} from "./inventory.js";
import {check} from "k6";
import {Rate} from "k6/metrics";
import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';

const searchQueriesArtist = new SharedArray("sqArtists", () => JSON.parse(open("./fixtures/search.json"))["artist"]);
const searchQueriesAlbum = new SharedArray("sqAlbums", () => JSON.parse(open("./fixtures/search.json"))["album"]);
const searchQueriesTrack = new SharedArray("sqTracks", () => JSON.parse(open("./fixtures/search.json"))["track"]);

export let errorRate = new Rate('errors');

export function handleSummary(data) {
  return {
    'stdout': textSummary(data, {indent: ' ', enableColors: true}),
    [`./results/${__ENV.environment}/inventory_2_search-${Date.now()}.json`] : JSON.stringify(data)
  }
}

export const options = {
  tags: {
    environment: __ENV.environment,
    test: 'inventory_2_search'
  },

  scenarios: {
    constant_vus: {
      executor: 'constant-vus',
      vus: 400,
      duration: '5m',

      tags: {
        scenario: 'constant_vus'
      }
    }
  },

  thresholds: {
    http_req_failed: ['rate<0.01'],
    errors: ['rate<0.01']
  }
}

export default function () {
  searchQueriesArtist.forEach(sq => {
    const res = findArtistsByName(sq.query, sq.page, sq.limit);
    const c1 = check(res, {
      "status is 200": val => val.status === 200,
      "response is not empty": val => val.json().length > 0
    });
    errorRate.add(!c1);
  });

  searchQueriesAlbum.forEach(sq => {
    const res = findAlbumsByName(sq.query, sq.page, sq.limit);
    const c2 = check(res, {
      "status is 200": val => val.status === 200,
      "response is not empty": val => val.json().length > 0
    });

    errorRate.add(!c2);
  });

  searchQueriesTrack.forEach(sq => {
    const res = findTracksByName(sq.query, sq.page, sq.limit);
    const c3 = check(res, {
      "status is 200": val => val.status === 200,
      "response is not empty": val => val.json().length > 0
    });

    errorRate.add(!c3);
  });
}