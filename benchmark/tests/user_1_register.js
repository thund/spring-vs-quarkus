import {createUser, getUserByUsername} from "./user.js";
import {check} from "k6";
import {Rate} from "k6/metrics";
import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';

export let errorRate = new Rate('errors');

export function handleSummary(data) {
  return {
    'stdout': textSummary(data, {indent: ' ', enableColors: true}),
    [`./results/${__ENV.environment}/user_1_register-${Date.now()}.json`] : JSON.stringify(data)
  }
}

export const options = {
  tags: {
    environment: __ENV.environment,
    test: 'user_1_register'
  },

  scenarios: {
    constant_vus: {
      executor: 'constant-vus',
      vus: 400,
      duration: '5m',

      tags: {
        scenario: 'constant_vus'
      }
    }
  },

  thresholds: {
    http_req_failed: ['rate<0.01'],
    errors: ['rate<0.01']
  }
}

export function getUser() {
  return {
    username: `johndoe-${__VU}-${__ITER}-${Date.now()}`,
    displayName: `John Doe (VU: ${__VU}, iter: ${__ITER}, date: ${Date.now()})`
  }
}

export default function () {
  const user = getUser();

  const registerRes = createUser(user);
  const c1 = check(registerRes, {
    "status is 201": val => val.status === 201,
    "returned correct user": val => {
      const json = val.json();
      return json['username'] === user.username && json['displayName'] === user.displayName
    }
  });

  errorRate.add(!c1);

  const getUserRes = getUserByUsername(user.username);
  const c2 = check(getUserRes, {
    "status is 200": val => val.status === 200,
    "returned correct user": val => {
      const json = val.json();
      return json['username'] === user.username && json['displayName'] === user.displayName
    }
  });

  errorRate.add(!c2);
}