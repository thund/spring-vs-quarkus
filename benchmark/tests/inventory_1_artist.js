import http from "k6/http";
import {
  getAlbumById,
  getAlbumArtists,
  getArtistAlbums,
  getTrackArtists,
  getAlbumTracks,
  getArtistById,
  getTrackById
} from "./inventory.js";
import {check, group} from "k6";
import {SharedArray} from "k6/data";
import { Rate } from 'k6/metrics';
import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';

const artists = new SharedArray("artists", () => JSON.parse(open("./fixtures/artists.json")));

export let errorRate = new Rate('errors');

export function handleSummary(data) {
  return {
    'stdout': textSummary(data, {indent: ' ', enableColors: true}),
    [`./results/${__ENV.environment}/inventory_1_artist-${Date.now()}.json`] : JSON.stringify(data)
  }
}

export const options = {
  tags: {
    environment: __ENV.environment,
    test: 'inventory_1_artist'
  },

  scenarios: {
    constant_vus: {
      executor: 'constant-vus',
      vus: 400,
      duration: '5m',

      tags: {
        scenario: 'constant_vus'
      }
    }
  },

  thresholds: {
    http_req_failed: ['rate<0.01'],
    errors: ['rate<0.01']
  }
}

export function getArtist() {
  const aLength = artists.length;
  return artists[((__VU % aLength) + aLength) % aLength];
}

const isJson = (res) => res.headers['Content-Type'] === 'application/json';

export default function() {
  const artistId = getArtist();

  const artistRes = getArtistById(artistId);
  const c1 = check(artistRes, {
    "status is 200": val => val.status === 200,
    "content-type is application/json": isJson
  });

  errorRate.add(!c1);

  const artistAlbumsRes = getArtistAlbums(artistId);
  const c2 = check(artistAlbumsRes, {
    "status is 200": val => val.status === 200,
    "content-type is application/json": isJson,
    "has album": res => res.json().length > 0
  });

  errorRate.add(!c2);

  const albumId = artistAlbumsRes.json()[0]['gid'];

  const albumRes = getAlbumById(albumId);
  const c3 = check(albumRes, {
    "status is 200": val => val.status === 200,
    "content-type is application/json": isJson
  });

  errorRate.add(!c3);

  const albumArtistRes = getAlbumArtists(albumId);
  const c4 = check(albumArtistRes, {
    "status is 200": val => val.status === 200,
    "content-type is application/json": isJson,
    "has artists": res => res.json().length > 0
  });

  errorRate.add(!c4);

  const albumTrackRes = getAlbumTracks(albumId);
  const c5 = check(albumTrackRes, {
    "status is 200": val => val.status === 200,
    "content-type is application/json": isJson,
    "has tracks": res => res.json().length > 0
  });

  errorRate.add(!c5);

  const trackId = albumTrackRes.json()[0]['gid'];

  const trackRes = getTrackById(trackId);
  const c6 = check(trackRes, {
    "status is 200": val => val.status === 200,
    "content-type is application/json": res => res.headers['Content-Type'] === 'application/json'
  });

  errorRate.add(!c6);

  const trackArtistRes = getTrackArtists(trackId);
  const c7 = check(trackArtistRes, {
    "status is 200": val => val.status === 200,
    "content-type is application/json": res => res.headers['Content-Type'] === 'application/json',
    "has artists": res => res.json().length > 0
  });

  errorRate.add(!c7);
}