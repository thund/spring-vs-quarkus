import {
  addTracksToPlaylist,
  createUser,
  createUserPlaylist, deletePlaylist, editPlaylist, getTracksOfPlaylist,
  getUserByUsername,
  getUserPlaylists, removeTracksToPlaylist
} from "./user.js";
import {check} from "k6";
import {SharedArray} from "k6/data";
import {Rate} from "k6/metrics";
import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';

export let errorRate = new Rate('errors');

export function handleSummary(data) {
  return {
    'stdout': textSummary(data, {indent: ' ', enableColors: true}),
    [`./results/${__ENV.environment}/user_3_playlist_tracks-${Date.now()}.json`] : JSON.stringify(data)
  }
}

export const options = {
  tags: {
    environment: __ENV.environment,
    test: 'user_3_playlist_tracks'
  },

  scenarios: {
    constant_vus: {
      executor: 'constant-vus',
      vus: 400,
      duration: '5m',

      tags: {
        scenario: 'constant_vus'
      }
    }
  },

  thresholds: {
    http_req_failed: ['rate<0.01'],
    errors: ['rate<0.01']
  }
}

const addTracks = new SharedArray("addTracks", () => JSON.parse(open("./fixtures/playlist_tracks.json"))['add']);
const delTracks = new SharedArray("delTracks", () => JSON.parse(open("./fixtures/playlist_tracks.json"))['delete']);

export function getUser() {
  return {
    username: `johndoe-${__VU}-${Date.now()}`,
    displayName: `John Doe (VU: ${__VU}, date: ${Date.now()})`
  }
}

export default function() {
  // TODO only call once per VU
  const user = getUser();
  const registerRes = createUser(user);
  const c1 = check(registerRes, {
    "status is 201": val => val.status === 201,
    "returned correct user": val => {
      const json = val.json();
      return json['username'] === user.username && json['displayName'] === user.displayName
    }
  });

  errorRate.add(!c1);

  const playlist1 = {
    name: "Drum and Bass",
    publicPlaylist: false,
    collaborative: false
  };
  const playlist1Res = createUserPlaylist(user.username, playlist1);
  const c2 = check(playlist1Res, {
    "status is 201": val => val.status === 201,
    "playlist name returned": val => val.json()['name'] === playlist1.name,
    "contains id": val => !!val.json()['id'],
    "is private": val => !val.json()['publicPlaylist']
  });

  errorRate.add(!c2);

  const { username } = user;
  const playlistId = playlist1Res.json()['id'];

  let addLength = 0;

  addTracks.forEach(chunk => {
    const res = addTracksToPlaylist(playlistId, chunk);
    addLength += chunk.length;
    const c3 = check(res, {
      "status is 200": val => val.status === 200,
      "response not empty": val => val.json().length !== 0
    });

    errorRate.add(!c3);
  });

  let playlistRes = getTracksOfPlaylist(playlistId);

  const c4 = check(playlistRes, {
    "status is 200": val => val.status === 200,
    "length is array length": val => val.json().length === addLength
  });

  errorRate.add(!c4);

  let delLength = 0;

  delTracks.forEach(chunk => {
    const res = removeTracksToPlaylist(playlistId, chunk);
    delLength += chunk.length;
    const c5 = check(res, {
      "status is 204": val => val.status === 204
    });
    errorRate.add(!c5);
  });

  playlistRes = getTracksOfPlaylist(playlistId);
  const c6 = check(playlistRes, {
    "status is 200": val => val.status === 200,
    "length is array length": val => val.json().length === addLength - delLength
  });

  errorRate.add(!c6);
}