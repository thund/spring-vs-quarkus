import {
  createUser,
  createUserPlaylist, deletePlaylist, editPlaylist,
  getUserByUsername,
  getUserPlaylists
} from "./user.js";
import {check} from "k6";
import {Rate} from "k6/metrics";
import { textSummary } from 'https://jslib.k6.io/k6-summary/0.0.1/index.js';

export let errorRate = new Rate('errors');

export function handleSummary(data) {
  return {
    'stdout': textSummary(data, {indent: ' ', enableColors: true}),
    [`./results/${__ENV.environment}/user_2_playlist-${Date.now()}.json`] : JSON.stringify(data)
  }
}

export const options = {
  tags: {
    environment: __ENV.environment,
    test: 'user_2_playlist'
  },

  scenarios: {
    constant_vus: {
      executor: 'constant-vus',
      vus: 400,
      duration: '5m',

      tags: {
        scenario: 'constant_vus'
      }
    }
  },

  thresholds: {
    http_req_failed: ['rate<0.01'],
    errors: ['rate<0.01']
  }
}

export function getUser() {
  return {
    username: `johndoe-${__VU}-${Date.now()}`,
    displayName: `John Doe (VU: ${__VU}, date: ${Date.now()})`
  }
}

export function setup() {

}

export default function() {
  // TODO only once per VU
  const user = getUser();
  const registerRes = createUser(user);
  const c1 = check(registerRes, {
    "status is 201": val => val.status === 201,
    "returned correct user": val => {
      const json = val.json();
      return json['username'] === user.username && json['displayName'] === user.displayName
    }
  });

  errorRate.add(!c1);

  const { username } = user;
  const userPlaylistsRes = getUserPlaylists(username);
  const c2 = check(userPlaylistsRes, {
    "status is 200": val => val.status === 200
  });

  errorRate.add(!c2);

  let playlist1 = {
    name: "Drum and Bass",
    publicPlaylist: false,
    collaborative: false
  };
  const playlist1Res = createUserPlaylist(username, playlist1);
  const c3 = check(playlist1Res, {
    "status is 201": val => val.status === 201,
    "playlist name returned": val => val.json()['name'] === playlist1.name,
    "contains id": val => !!val.json()['id'],
    "is private": val => !val.json()['publicPlaylist']
  });

  errorRate.add(!c3);

  const playlist2 = {
    name: "Drum and Bass 2",
    publicPlaylist: true,
    collaborative: true
  };
  const playlist2Res = createUserPlaylist(username, playlist2);
  const c4 = check(playlist2Res, {
    "status is 201": val => val.status === 201,
    "playlist name returned": val => val.json()['name'] === playlist2.name,
    "contains id": val => !!val.json()['id']
  });

  errorRate.add(!c4);

  const playlist1Id = playlist1Res.json()['id'];
  const playlist2Id = playlist2Res.json()['id'];

  const deletePlaylistRes = deletePlaylist(playlist2Id);
  const c5 = check(deletePlaylistRes, {
    "status is 204": val => val.status === 204
  });

  errorRate.add(!c5);

  playlist1.publicPlaylist = true;
  const editPlaylistRes = editPlaylist(playlist1Id, playlist1);
  const c6 = check(editPlaylistRes, {
    "status is 200": val => val.status === 200,
    "is public": val => val.json()['publicPlaylist']
  });

  errorRate.add(!c6);
}