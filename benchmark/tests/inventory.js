import http from "k6/http";
import {BASE_URL_INVENTORY} from "./common.js";

export function getArtistById(id) {
  return http.get(`${BASE_URL_INVENTORY}/artists/${id}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getArtistById"
    }
  })
}

export function getArtistAlbums(id) {
  return http.get(`${BASE_URL_INVENTORY}/artists/${id}/albums`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getArtistAlbums"
    }
  })
}

export function getAlbumById(id) {
  return http.get(`${BASE_URL_INVENTORY}/albums/${id}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getAlbumById"
    }
  })
}

export function getAlbumTracks(id) {
  return http.get(`${BASE_URL_INVENTORY}/albums/${id}/tracks`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getAlbumTracks"
    }
  })
}

export function getAlbumArtists(id) {
  return http.get(`${BASE_URL_INVENTORY}/albums/${id}/artists`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getAlbumArtists"
    }
  })
}

export function getTrackById(id) {
  return http.get(`${BASE_URL_INVENTORY}/tracks/${id}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getTrackById"
    }
  })
}

export function getTrackArtists(id) {
  return http.get(`${BASE_URL_INVENTORY}/tracks/${id}/artists`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "getTrackArtists"
    }
  })
}

export function findTracksByName(query, page = 0, limit = 25) {
  return http.get(`${BASE_URL_INVENTORY}/tracks/search?q=${query}&page=${page}&limit=${limit}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "findTracksByName"
    }
  })
}

export function findAlbumsByName(query, page = 0, limit = 25) {
  return http.get(`${BASE_URL_INVENTORY}/albums/search?q=${query}&page=${page}&limit=${limit}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "findAlbumsByName"
    }
  })
}

export function findArtistsByName(query, page = 0, limit = 25) {
  return http.get(`${BASE_URL_INVENTORY}/artists/search?q=${query}&page=${page}&limit=${limit}`, {
    headers: {
      "Accept": "application/json"
    },
    tags: {
      "name": "findArtistsByName"
    }
  })
}