#!/usr/bin/env bash

PS3='Pick the environment: '
options=("Spring" "Spring Native" "Quarkus" "Quarkus Native" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Spring")
            implementation='spring'
            break;
            ;;
        "Spring Native")
            implementation='spring-native'
            break;
            ;;
        "Quarkus")
            implementation='quarkus'
            break;
            ;;
        "Quarkus Native")
            implementation='quarkus-native'
            break;
            ;;
        "Quit")
            exit 0;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

printf "\n================================================\n"
printf "Make sure that the environment is running\n"
printf 'Press any key to continue...'\n;
printf "=================================================\n"
read  -n 1

printf "\n================================================\n"
printf "Test 1 Inventory Service\n"
printf "=================================================\n"

k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/inventory_1_artist.js

printf "\n================================================\n"
printf "Test 2 Inventory Search\n"
printf "=================================================\n"

k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/inventory_2_search.js

printf "\n================================================\n"
printf "Test 1 User Service\n"
printf"=================================================\n"

k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/user_1_register.js

printf "\n================================================\n"
printf "Test 2 User Service\n"
printf"=================================================\n"

k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/user_2_playlist.js

printf "\n================================================\n"
printf "Test 3 User Service\n"
printf"=================================================\n"

k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/user_3_playlist_tracks.js