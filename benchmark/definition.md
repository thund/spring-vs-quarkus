# SLI (Service Level Indicators)
- request latency
- system throughput

# Was ist testbar?

## Inventory Service

| Testbar | API | Beschreibung |
| ------- | --- | ------------ |
| Nein | GET /search | Wird timeouten - benötigt nen Elasticsearch oder Melisearch als Indexierung |
| Ja   | GET /artist/{id} | |
| Ja   | GET /artist/{id}/albums | |
| Ja   | GET /albums/{id} | |
| Ja   | GET /albums/{id}/tracks | |
| Ja   | GET /albums/{id}/artists | |
| Ja   | GET /tracks/{id} | |
| Ja   | HEAD /tracks/{id} | Sinnvoll? |
| Ja   | GET /tracks/{id}/artists | |

## User Service

| Testbar | API | Beschreibung |
| ------- | --- | ------------ |
| Ja | POST /users | |
| Ja | GET /users/{username} | |
| Ja | GET /users/{username}/playlists | |
| Ja | POST /users/{username}/playlists | |
| Ja | GET /playlists/{id} | |
| Ja | PUT /playlists/{id} | |
| Ja | DELETE /playlists/{id} | |
| Ja | GET /playlists/{id}/tracks | |
| Ja | POST /playlists/{id}/tracks | |
| Ja | DELETE /playlists/{id}/tracks | |
| Ja | GET /playlists/search | |

# Szenarien

## Inventory Service

### Szenario 1: Informationen über einen Künstler einholen

1. GET /artists/{id}
2. GET /artists/{id}/albums
3. GET /albums/{id}
4. GET /albums/{id}/tracks
5. GET /albums/{id}/artists
6. GET /tracks/{id}
7. GET /tracks/{id}/artists

## User Service

### Szenario 1: User registriert sich und ruft seine Infos ab

Ablauf:
User registriert sich und ruft daraufhin seine eigenen Infos ab.

1. POST /users
2. GET /users/{username}

### Szenario 2: User ruft seine Playlisten ab

Ablauf:
User ruft seine Playlists ab, erstellt zwei Playlisten, bearbeitet eine und löscht eine

1. GET /users/{username}/playlists
2. POST /users/{username}/playlists
3. POST /users/{username}/playlists
4. PUT /playlists/{id}
5. DELETE /playlists/{id}

### Szenario 3: User füllt Playlist

Ablauf:
User erstellt Playlist und füllt sie danach Stück für Stück mit Titeln

1. POST /users/{username}/playlists
2. POST /playlists/{id}/tracks (x Wdh.)
3. DELETE /playlists/{id}/tracks