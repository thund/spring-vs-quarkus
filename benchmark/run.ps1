$implementation;

do {
    Write-Host "`n============= Pick the environment=============="
    Write-Host "`t1. '1' Spring"
    Write-Host "`t2. '2' Spring Native"
    Write-Host "`t3. '3' Quarkus"
    Write-Host "`t4. '4' Quarkus Native"
    Write-Host "`t5. 'Q' to Quit"
    Write-Host "=================================================="
    $choice = Read-Host "`nEnter Choice"
} until (($choice -eq '1') -or ($choice -eq '2') -or ($choice -eq '3') -or ($choice -eq '4') -or ($choice -eq 'Q') )
switch ($choice) {
    '1'{
        $implementation = "spring";
    }
    '2'{
        $implementation = "spring-native";
    }
    '3'{
        $implementation = "quarkus";
    }
    '4'{
        $implementation = "quarkus-native";
    }
    'Q'{
        Return
    }
}

Write-Host "`n================================================"
Write-Host "Make sure that the environment is running"
Write-Host 'Press any key to continue...';
Write-Host "================================================="
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

Write-Host "Start: "
Write-Host (Get-Date -Format "o")

Write-Host "`n================================================"
Write-Host "Test 1 Inventory Service"
Write-Host (Get-Date -Format "o")
Write-Host "================================================="

$timestamp = Get-Date -UFormat %s -Millisecond 0
k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/inventory_1_artist.js

Write-Host "`n================================================"
Write-Host "Test 2 Inventory Search"
Write-Host (Get-Date -Format "o")
Write-Host "================================================="

$timestamp = Get-Date -UFormat %s -Millisecond 0
k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/inventory_2_search.js

Write-Host "`n================================================"
Write-Host "Test 1 User Service"
Write-Host (Get-Date -Format "o")
Write-Host "================================================="

$timestamp = Get-Date -UFormat %s -Millisecond 0
k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/user_1_register.js

Write-Host "`n================================================"
Write-Host "Test 2 User Service"
Write-Host (Get-Date -Format "o")
Write-Host "================================================="

$timestamp = Get-Date -UFormat %s -Millisecond 0
k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/user_2_playlist.js

Write-Host "`n================================================"
Write-Host "Test 3 User Service"
Write-Host (Get-Date -Format "o")
Write-Host "================================================="

$timestamp = Get-Date -UFormat %s -Millisecond 0
k6 run -e environment=$implementation -o influxdb=http://localhost:8086/k6 ./tests/user_3_playlist_tracks.js

Write-Host "End: "
Write-Host (Get-Date -Format "o")